/**
 * Copyright (C) 2022-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { EmacsManager } from "..";

export class EditorFileLoadedListener {

    private static LISTENER_FIELD_NAME = "emacsEditorFileLoadedListener"
    readonly listenerFieldName = EditorFileLoadedListener.LISTENER_FIELD_NAME
    private enabled: boolean;
    emacsManager: EmacsManager
    private applyFunc = mxUtils.bind(this, this.apply)

    constructor(emacsManager: EmacsManager) {
        this.emacsManager = emacsManager
    }

    apply(_editor: any, eventObject: mxEventObject) {
        if (eventObject.name !== "fileLoaded") {
            console.log("[BUG] eventObject.name !== fileLoaded: " + eventObject.name)
        }
        if (this.isEnabled()) {
             this.emacsManager.eaasMkEaasDataConsoleLog("eaasFileLoaded")
        }
    }

    install() {
        const installedFunc = this.getInstalledListenerFuncMaybe()
        if (!installedFunc) {
            this.emacsManager.editorUi.editor.addListener("fileLoaded", this.applyFunc)
        }
    }

    uninstall() {
        this.disable()
        this.emacsManager.editorUi.editor.removeListener(this.applyFunc)
    }

    isEnabled() {
        return this.enabled
    }

    private setEnabled(value: boolean) {
        this.enabled = value
    }

    public getInstalledListenerFuncMaybe() {
        const editor = this.emacsManager.editorUi.editor
        const installedListenerFuncMaybe = editor.eventListeners.filter(
            (nameOrFunc) => nameOrFunc === this.applyFunc
        )
        if (installedListenerFuncMaybe.length === 0) {
            return;
        } else if (installedListenerFuncMaybe.length === 1) {
            return installedListenerFuncMaybe[0]
        } else {
            throw new Error("[ERROR-BUG-FOUND] installedListener.length > 1")
        }
    }

    enable() {
        this.install()
        this.setEnabled(true)
    }

    disable() {
        this.setEnabled(false)
    }
}
