;;; emacs-drawio-mode.el --- Minor mode for the emacs-drawio buffer.

;;; Commentary:
;;

(require 'subr-x)

;;; Code:

(defvar emacs-drawio-mode-map (make-sparse-keymap))

(defvar emacs-drawio-mode-paragraph-fill-step 2)

(defun emacs-drawio-mode-paragraph-get-width ()
  "Get width in columns of current paragraph."
  (let ((lines (split-string
                (substring-no-properties
                 (thing-at-point 'paragraph))
                "\n" t))
        (current-width 0))
    (dolist (l lines)
      (let ((line-length (length l)))
        (when (> line-length current-width)
          (setq current-width line-length))))
    current-width))

(defun emacs-drawio-mode-paragraph-fill-apply-change (delta)
  "Set new `fill-column' and call `fill-paragraph'.
New value for `fill-column' is calculated adding DELTA to either
current non-zero value of `fill-column' or, if zero, current
width of paragraph."
  (let ((new-fill-column
         (+ delta (if (< fill-column 0)
                      (emacs-drawio-mode-paragraph-get-width)
                    fill-column)))
        (save-deactivate-mark deactivate-mark))
    (when (< new-fill-column 0)
      (setq new-fill-column 0))
    (set-fill-column new-fill-column)
    (if mark-active
        (progn (setq deactivate-mark nil)
               (fill-paragraph nil (region-bounds))
               (setq deactivate-mark save-deactivate-mark))
      (fill-paragraph))))

(defun emacs-drawio-mode-paragraph-fill-decrease ()
  "Decrease width of paragraph."
  (interactive)
  (emacs-drawio-mode-paragraph-fill-apply-change
   (- emacs-drawio-mode-paragraph-fill-step)))

(defun emacs-drawio-mode-paragraph-fill-increase ()
  "Incrase width of paragraph."
  (interactive)
  (emacs-drawio-mode-paragraph-fill-apply-change
   emacs-drawio-mode-paragraph-fill-step))

(define-minor-mode emacs-drawio-mode
  "Minor mode for *emacs-drawio* buffer."
  :require 'org-mode
  :keymap emacs-drawio-mode-map)

(provide 'emacs-drawio-mode)

;;; emacs-drawio-mode.el ends here
