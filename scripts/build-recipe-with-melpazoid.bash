#!/bin/env bash

(
git clone "https://github.com/riscy/melpazoid.git"
cd melpazoid
RECIPE='(emacs-drawio :fetcher gitlab :repo "spellcard199/emacs-drawio" :files ("elisp/*.el" "js")))' make
)
