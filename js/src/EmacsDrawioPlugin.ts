/**
 * Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { EmacsManager } from "./emacsmanager/EmacsManager";
import * as util from "./util";

export class EmacsDrawioPlugin {

    EmacsManager: typeof EmacsManager = EmacsManager;
    emacsManager: EmacsManager
    util = util;
    private static WINDOW_FIELD = "emacsDrawio"

    constructor(emacsManager: EmacsManager) {
        this.emacsManager = emacsManager
    }

    static loadPlugin(): EmacsDrawioPlugin | undefined {
        let emacsDrawio: EmacsDrawioPlugin;
        //@ts-ignore
        Draw.loadPlugin(function(editorUi: any) {
            if (window[EmacsDrawioPlugin.WINDOW_FIELD]) {
                (window[EmacsDrawioPlugin.WINDOW_FIELD] as EmacsDrawioPlugin).emacsManager.quit();
            }
            const emacsManager = EmacsManager.new(editorUi);
            emacsDrawio = new EmacsDrawioPlugin(emacsManager)
            window[EmacsDrawioPlugin.WINDOW_FIELD] = emacsDrawio
            emacsManager.enable();
        });
        return emacsDrawio
    }
}
