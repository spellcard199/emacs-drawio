#!/bin/env bash

set -euo pipefail
shopt -s inherit_errexit

HERE_DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"
APPIMAGE_EXECUTABLE="$HERE_DIR/draw.io-x86_64-13.9.9.AppImage"

ELECTRON_DRAWIO_DIR="$HOME/Downloads/drawio-desktop/"
ELECTRON_WEBAPP_DIR="$ELECTRON_DRAWIO_DIR/drawio/src/main/webapp/"

launch_appimage() {
    "$APPIMAGE_EXECUTABLE" "$@"
}

launch_electron() {
    (
        cd "$ELECTRON_WEBAPP_DIR" \
            && npx electron ./electron.js "$@"
    )
}

export DRAWIO_DISABLE_UPDATE=true
set -- "--lang=en" "--create" "$@"
# launch_electron "$@"
launch_appimage "$@"
