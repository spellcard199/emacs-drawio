;;; package --- custom configuration for emacs.

;;; Commentary:
;; Emacs custom configuration and startup script.

;;; Code:

(setq user-init-file (or load-file-name (buffer-file-name)))
(setq user-emacs-directory (file-name-directory user-init-file))

(require 'evil)
(require 'evil-escape)
(require 'shr)
(require 'shr-color)

;; Appearance
(load-theme 'deeper-blue)
(set-background-color "black")
(setq visible-bell nil)
(menu-bar-mode -1)
(tool-bar-mode -1)
(setq shr-color-visible-luminance-min 100)
(setq-default indent-tabs-mode nil)

;; Evil
(setq evil-escape-key-sequence "jk")
(evil-mode 1)
(evil-escape-mode 1)
(evil-set-undo-system 'undo-redo)

;; Buffer formatting
(defun indent-buffer-delete-trailing-whitespace ()
  "Indent and delete trailing whitespace in current buffer."
  (interactive)
  (indent-region (point-min) (point-max))
  (delete-trailing-whitespace))
(define-key global-map
  (kbd "C-ò C-M-\\")
  #'indent-buffer-delete-trailing-whitespace)

;; Emacs lisp mode
(define-key emacs-lisp-mode-map
  (kbd "C-c C-c")
  #'eval-defun)

(require 'use-package)

(use-package flycheck
  :hook
  (prog-mode . flycheck-mode))

(use-package company
  :hook (prog-mode . company-mode)
  :config (setq company-tooltip-align-annotations t)
  (setq company-minimum-prefix-length 1))

;; Typescript
(require 'tide)
(use-package tide
  ;; :ensure t
  :after (typescript-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
         (typescript-mode . tide-hl-identifier-mode)
         (before-save . tide-format-before-save)))

;; Indium
(require 'indium)
(define-key indium-interaction-mode-map
  (kbd "C-c C-c")
  #'indium-eval-defun)

;; Customize flycheck
(setq flycheck-emacs-lisp-load-path 'inherit)

(require 'emacs-drawio-eaas)
(evil-define-key 'normal org-mode-map (kbd "SPC qq")
  #'emacs-drawio-eaas-org--to-app-async--queue-run)
(evil-define-key 'normal org-mode-map (kbd "SPC qw")
  #'emacs-drawio-eaas-edit-selection-cells-done-editing)
(evil-define-key 'normal org-mode-map (kbd "SPC qa")
  #'emacs-drawio-eaas-to-app-async-autosync-toggle)
(evil-define-key 'normal org-mode-map (kbd "SPC fd")
  #'emacs-drawio-eaas-edit-selection-cells-frame-toggle-decorations)

(provide 'init)
;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(evil-undo-system 'undo-redo))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
