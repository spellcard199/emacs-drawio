/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { EmacsManager } from "../EmacsManager";
import { EmacsGraphManager } from "../graph/EmacsGraphManager";

interface mxListenerObject {
    name: string
    f: Function
}

export class EditorUiCustomListener {

    private static LISTENER_FIELD_NAME = "emacsEditorUiCustomListener"
    emacsManager: EmacsManager;
    private enabled: boolean = true;
    tmpGraphs: Map<mxGraph, EmacsGraphManager> = new Map();

    constructor(emacsManager: EmacsManager) {
        this.emacsManager = emacsManager
    }

    isEnabled() {
        return this.enabled
    }

    private setEnabled(boolean: boolean) {
        this.enabled = boolean;
    }

    apply(_eventObject: KeyboardEvent) {
        // Triggered after each keypress: if graph and selection model do not
        // have listeners:
        // 1. Add them
        // 2. Set the graph[LISTENER_FIELD_NAME] attr to non-undefined: on keypress
        //    we are using it to know if the graph object has changed and we need
        //    to install EmacsGraphManager.
        if (this.isEnabled()) {
            this.enableCurrentEmacsGraphManager();
        }
    }

    mkListener() {
        // wrapping 'this.apply' because just 'this' does not get called
        const func = (evt: KeyboardEvent) => { this.apply(evt) };
        func[EditorUiCustomListener.LISTENER_FIELD_NAME] = this;
        return func;
    }

    static getMxListenerObject(editorUiCustomListener: EditorUiCustomListener): mxListenerObject | undefined {
        const mxListenerList = editorUiCustomListener.emacsManager.editorUi.container.mxListenerList
        if (!mxListenerList) {
            return undefined
        }
        const previouslyInstalledListener = mxListenerList.filter(
            (l: mxListenerObject) => l.f[EditorUiCustomListener.LISTENER_FIELD_NAME] === this
        )
        if (previouslyInstalledListener.length === 0) {
            return undefined;
        } else if (previouslyInstalledListener.length === 1) {
            return previouslyInstalledListener[0]
        } else {
            throw new Error("[ERROR-BUG-FOUND] previoslyInstalledListener.length > 1")
        }
    }

    enable() {
        this.install();
        this.enableCurrentEmacsGraphManager();
        this.setEnabled(true);
    }

    disable() {
        const graph: mxGraph = this.emacsManager.editorUi.editor.graph;
        const emacsGraphManagerMaybe: EmacsGraphManager | undefined = EmacsGraphManager.getGraphManager(graph);
        if (typeof emacsGraphManagerMaybe !== "undefined") {
            emacsGraphManagerMaybe.disable();
        }
        this.setEnabled(false);
    }

    install() {
        const listenerObjectMaybe: mxListenerObject | undefined = EditorUiCustomListener.getMxListenerObject(this);
        if (typeof listenerObjectMaybe === "undefined") {
            const editorUi = this.emacsManager.editorUi
            // Listener ends up in document.body.mxListenerList: you can also remove it from there.
            mxEvent.addListener(this.emacsManager.editorUi.container, "keypress", this.mkListener())
        }
    }

    uninstall() {
        this.disable();
        const graph: mxGraph = this.emacsManager.editorUi.editor.graph;
        EmacsGraphManager.uninstallEmacsGraphManager(graph);
        const editorUiContainer: any = this.emacsManager.editorUi.container;
        const listenerObject = EditorUiCustomListener.getMxListenerObject(this);
        if (typeof listenerObject !== "undefined") {
            mxEvent.removeListener(editorUiContainer, "keypress", listenerObject.f)
        }
    }

    enableCurrentEmacsGraphManager() {
        const graph: mxGraph = this.emacsManager.editorUi.editor.graph;
        const emacsGraphManager = EmacsGraphManager.getInstallEmacsGraphManager(graph, this);
        emacsGraphManager.enable();
        this.tmpGraphs.set(graph, emacsGraphManager);
        if (this.tmpGraphs.size > 1) {
            const nonCurrentGraphs: mxGraph[] = [];
            for (let g of this.tmpGraphs.keys()) {
                if (g !== graph) {
                    nonCurrentGraphs.push(g);
                }
            }
            nonCurrentGraphs.forEach((g: mxGraph) => {
                EmacsGraphManager.uninstallEmacsGraphManager(g);
                this.tmpGraphs.delete(g)
            });
        }
    }

}
