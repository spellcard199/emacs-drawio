;;; emacs-drawio-utils.el --- utility functions -*- lexical-binding:t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;

(require 'cl-lib)

;;; Code:

(defun emacs-drawio-util-directory-files-js (dir)
  "List files ending in .js inside `DIR'."
  (directory-files dir t "[^.]\\.js\\'"))

(defun emacs-drawio-util--remove-compilation-finish-function (func)
  "Remove FUNC from `compilation-finish-functions'."
  ;; Using `delete' because `remove-hook' does not work here.
  (setq compilation-finish-functions
        (delete func compilation-finish-functions)))

(defun emacs-drawio-util--string-overlapping-ends
    (str-with-suffix str-with-prefix)
  "Return overlapping ends of 2 strings.
Example: The following returns \"fr\":
 (emacs-drawio-util--string-overlapping-ends \"Array.fr\" \"from\")
Argument STR-WITH-SUFFIX is a string whose suffix can match
STR-WITH-PREFIX's prefix.
Argument STR-WITH-PREFIX is a string whose prefix can match
STR-WITH-SUFFIX's suffix."
  (cl-do ((s str-with-suffix
             (substring s 1 (length s))))
      ((string-prefix-p s str-with-prefix)
       s)))

(defmacro emacs-drawio-util-measure-time (&rest body)
  "Measure the time it takes to evaluate BODY."
  `(let ((time (current-time)))
     ,@body
     (message "%.06f" (float-time (time-since time)))))

(defun emacs-drawio-util-replace-regexp-in-buffer (buffer regexp rep)
  "Replace REGEXP in BUFFER, REP times."
  (interactive (list (current-buffer)
                     (read-string "regexp: ")
                     (read-string "rep: ")))
  (with-current-buffer buffer
    (let ((new-buffer-content
           (replace-regexp-in-string
            regexp rep
            (buffer-substring-no-properties (point-min) (point-max)))))
      (insert (prog1 new-buffer-content (erase-buffer))))))

;; BEGIN_OF Source: https://gist.github.com/inouetmhr/4116307

(defun emacs-drawio-util-base64-to-base64url (str)
  "Escape base64 STR to base64url."
  (setq str (replace-regexp-in-string "=+$" "" str))
  (setq str (replace-regexp-in-string "+" "-" str))
  (setq str (replace-regexp-in-string "/" "_" str)))

(defun emacs-drawio-util-base64url-to-base64 (str)
  "Unescape base64url STR to base64."
  (setq str (replace-regexp-in-string "-" "+" str))
  (setq str (replace-regexp-in-string "_" "/" str))
  (let ((mod (% (length str) 4)))
    (cond
     ((= mod 1) (concat str "==="))
     ((= mod 2) (concat str "=="))
     ((= mod 3) (concat str "="))
     (t str))))

(defun emacs-drawio-util-base64url-encode-string (str)
  "Encode STR to base64url."
  (emacs-drawio-util-base64-to-base64url (base64-encode-string str t)))

(defun emacs-drawio-util-base64url-decode-string (str)
  "Decode base64url STR."
  (base64-decode-string (emacs-drawio-util-base64url-to-base64 str)))

;; END_OF Source: https://gist.github.com/inouetmhr/4116307

(provide 'emacs-drawio-util)

;;; emacs-drawio-util.el ends here
