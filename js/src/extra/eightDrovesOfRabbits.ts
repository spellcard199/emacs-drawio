/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { DrawioPluginEightDrovesOfRabbits } from "drawio-plugin-eight-droves-of-rabbits/src/DrawioPluginEightDrovesOfRabbits"
import { KeyBinding } from "drawio-plugin-eight-droves-of-rabbits/src/ui/keyboard/KeyBinding";
import { Command } from "drawio-plugin-eight-droves-of-rabbits/src/ui/modal/Command";
import { NormalState } from "drawio-plugin-eight-droves-of-rabbits/src/ui/modal/vilikemode/states/normal/NormalState";
import { EmacsDrawioPlugin } from "../EmacsDrawioPlugin";


export function edrLoadCustom(emacsDrawioPlugin: EmacsDrawioPlugin) {
    // Load and Customize edr
    const emacsDrawioEditSelectionCells: Command<NormalState> = {
        name: "emacsDrawioEditSelectionCells",
        doc: "",
        run: (kew, _normalState) => {
            emacsDrawioPlugin.emacsManager.editSelectionCells();
            kew.stopPropagation = true;
        },
    }
    const emacsDrawioVertexInsertSelectEdit: Command<NormalState> = {
        name: "emacsDrawioVertexInsertSelectEdit",
        doc: "",
        run: (kew, normalState) => {
            // Insert vertex, select it and console.log('eaas-edit-selection-cells')
            const euim = normalState.viLikeMode.editorUiManager;
            const graph: mxGraph = euim.getCurrentGraph()
            graph.getModel().beginUpdate()
            let vert: mxCell
            try {
                vert = euim.insertDefaultVertex(graph)
            } finally {
                graph.getModel().endUpdate()
            }
            if (vert) {
                graph.setSelectionCell(vert);
                emacsDrawioPlugin.emacsManager.editSelectionCells();
            }
            kew.stopPropagation = true;
        },
    }
    const emacsDrawioEdgesInsertSelectEdit: Command<NormalState> = {
        name: "emacsDrawioEdgesInsertSelectEdit",
        doc: "",
        run: (kew, normalState) => {
            // Insert edge, select it and console.log('eaas-edit-selection-cells')
            const euim = normalState.viLikeMode.editorUiManager;
            const g = normalState.viLikeMode.editorUiManager.editorUi.editor.graph
            const edges: mxCell[] = euim.insertDefaultEdges(g)
            const graph: mxGraph = euim.getCurrentGraph()
            graph.setSelectionCells(edges);
            emacsDrawioPlugin.emacsManager.editSelectionCells();
            kew.stopPropagation = true;
        }
    }

    const edr = DrawioPluginEightDrovesOfRabbits.loadPlugin()
    edr.euim.disable()
    const vlm = edr.ui.modal.vilikemode.ViLikeMode.getInstance(edr.euim)
    if (vlm) {
        [
            [["i"], emacsDrawioEditSelectionCells],
            [["V"], emacsDrawioVertexInsertSelectEdit],
            [["E"], emacsDrawioEdgesInsertSelectEdit],
        ].map(KeyBinding.fromTuple).forEach((kb: KeyBinding<NormalState>) => {
            // Insert our keybindings before default ones.
            vlm.normal.keySeqMap.splice(0, 0, kb)
        })
    }
}
