#!/usr/bin/env bash

set -eax

NODE_VERSION="v18.16.0"

[ -s "$NVM_DIR/nvm.sh" ] \
    && \. "$NVM_DIR/nvm.sh" \
    && nvm install "$NODE_VERSION" # && nvm use "$NODE_VERSION"
