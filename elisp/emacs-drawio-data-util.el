;;; emacs-drawio-data-util.el --- Functions that work on emacs-drawio's data structures -*- lexical-binding: t; -*-


;; Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;

(require 'cl-lib)
(require 'emacs-drawio-data)
(require 'emacs-drawio-org)
(require 'json)

;;; Code:

(defun emacs-drawio-data-util-diagram-json-encode (diagram-data)
  "Encode DIAGRAM-DATA to json string."
  (json-encode
   (emacs-drawio-data-util-diagram-to-protocol diagram-data)))

(defun emacs-drawio-data-util-diagram-to-protocol (diagram-data)
  "Convert DIAGRAM-DATA to structure ready to be encoded to json."
  (mapcar
   #'emacs-drawio-data-util-page-to-protocol
   (emacs-drawio-data-diagram-pages diagram-data)))

(defun emacs-drawio-data-util-diagram-add-page (diagram-data page-data)
  "Add PAGE-DATA to DIAGRAM-DATA."
  (cl-check-type diagram-data emacs-drawio-data-diagram)
  (cl-check-type page-data emacs-drawio-data-page)
  (setf (emacs-drawio-data-diagram-pages diagram-data)
        (append (emacs-drawio-data-diagram-pages diagram-data)
                (list page-data))))

(defun emacs-drawio-data-util-page-to-protocol (page-data)
  "Convert PAGE-DATA to structure ready to be encoded to json."
  (cons (emacs-drawio-data-page-page-name page-data)
        (emacs-drawio-data-util-cells-ht-to-protocol
         (emacs-drawio-data-page-cells-ht page-data))))

(defun emacs-drawio-data-util-page-to-cons (page-data)
  "Convert PAGE-DATA to cons."
  (cons (emacs-drawio-data-page-page-name page-data)
        (emacs-drawio-data-page-cells-ht page-data)))

(cl-defmethod eamcs-drawio-data-util-page-add-cell
  ((page-data emacs-drawio-data-page)
   (cell-data emacs-drawio-data-cell))
  "Add CELL-DATA to PAGE-DATA."
  (puthash (emacs-drawio-data-cell-id cell-data)
           cell-data
           (emacs-drawio-data-page-cells-ht page-data)))

(cl-defmethod emacs-drawio-data-util-page-add-cell
  ((page-data emacs-drawio-data-page)
   (cell-string string))
  "Add CELL-STRING to PAGE-DATA."
  (emacs-drawio-org-data-page--add-cell page-data cell-string))

(defun emacs-drawio-data-util-cells-ht-to-protocol (cell-data-ht)
  "Convert CELL-DATA-HT to structure ready to be encoded to json.
Argument CELL-DATA-HT is an hash-table of
  cell-id -> cell-data-or-string."
  (cl-check-type cell-data-ht hash-table)
  (let ((cell-protocol-list))
    (maphash
     (lambda (_cell-id cell-data-or-string)
       (push
        (emacs-drawio-data-util-cell-to-protocol
         (cond ((emacs-drawio-data-cell-p cell-data-or-string)
                cell-data-or-string)
               ((stringp cell-data-or-string)
                (emacs-drawio-org-parse-org-cell cell-data-or-string))
               (t (error "Value in cell-data-ht's is neither a string \
nor `emacs-drawio-data-cell': %S"
                         cell-data-or-string))))
        cell-protocol-list))
     cell-data-ht)
    (apply 'vector cell-protocol-list)))

(defun emacs-drawio-data-util-cell-to-protocol (cell-data)
  "Convert CELL-DATA to structure ready to be encoded to json."
  (cl-check-type cell-data emacs-drawio-data-cell)
  `((id ,@(emacs-drawio-data-cell-id cell-data))
    (geometry ,@(emacs-drawio-data-cell-geometry cell-data))
    (style ,@(emacs-drawio-data-cell-style cell-data))
    (value ,(emacs-drawio-data-cell-value cell-data))))

(provide 'emacs-drawio-data-util)

;;; emacs-drawio-data-util.el ends here
