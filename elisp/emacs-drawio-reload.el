;;; emacs-drawio-reload.el --- Functions related to reloading js into diagramming app

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;

(require 'indiumeval)
(require 'emacs-drawio-global)
(require 'emacs-drawio-util)
(require 'emacs-drawio-compile)
(require 'emacs-drawio-eaas)

;;; Code:

(defvar emacs-drawio-reload-post-reload-hook
  `(,#'emacs-drawio-eaas-indium-client-log-hook-add
    ,#'emacs-drawio-eaas-org--after-change-func--add-hook)
  "Nullary functions to be called after reload.")

(cl-defun emacs-drawio-reload-load-webpack-bundle
    (&optional (bundle-file emacs-drawio-global-bundle-file))
  (interactive)
  (indiumeval-load-file bundle-file))

(defun emacs-drawio-reload--inject-js ()
  (emacs-drawio-reload-load-webpack-bundle)
  (dolist (f emacs-drawio-reload-post-reload-hook)
    (funcall f)))

(defun emacs-drawio-reload--compilation-finish-func (_buf _desc)
  (unwind-protect (emacs-drawio-reload--inject-js)
    ;; When done compiling and reloading, this function removes itself
    ;; from the `compilation-finish-function' hooks.
    (emacs-drawio-util--remove-compilation-finish-function
     #'emacs-drawio-reload--compilation-finish-func)))

(cl-defun emacs-drawio-reload (&optional (emacs-drawio-dir emacs-drawio-global-dir))
  (interactive)
  (if (not (process-live-p emacs-drawio-compile-watcher))
      (emacs-drawio-compile #'emacs-drawio-reload--compilation-finish-func
                            emacs-drawio-dir)
    (while (not (emacs-drawio-compile-watcher-compilation-finished-p))
      (sit-for 0.3))
    (emacs-drawio-reload--inject-js)))

(provide 'emacs-drawio-reload)
;;; emacs-drawio-reload.el ends here
