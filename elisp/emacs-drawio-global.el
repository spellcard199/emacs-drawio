;;; emacs-drawio-global.el --- Global variables -*- lexical-binding:t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>

;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;

(require 'indium)

;;; Code:

(defvar emacs-drawio-global-user-dir "~/.emacsDrawio.d")

(defconst emacs-drawio-global-dir-elisp
  (file-name-directory (or load-file-name (buffer-file-name)))
  "Directory containing elisp files.")

(defconst emacs-drawio-global-dir
  (if (string-suffix-p "elisp/" emacs-drawio-global-dir-elisp)
      (expand-file-name "../" emacs-drawio-global-dir-elisp)
    emacs-drawio-global-dir-elisp)
  "Directory root for emacs-drawio.")

(defconst emacs-drawio-global-dir-js
  (expand-file-name "js/" emacs-drawio-global-dir))

(defconst emacs-drawio-global-built-dir
  (expand-file-name "built/" emacs-drawio-global-dir-js))

(defconst emacs-drawio-global-dist-dir
  (expand-file-name "dist/" emacs-drawio-global-dir-js))

(defconst emacs-drawio-global-bundle-file
  (expand-file-name "bundle.js" emacs-drawio-global-dist-dir))

(setq indium-client-executable
      (expand-file-name "node_modules/indium/bin/indium"
                        emacs-drawio-global-dir-js))

(provide 'emacs-drawio-global)
;;; emacs-drawio-global.el ends here
