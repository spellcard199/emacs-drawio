;;; run-emacs-drawio.el --- Startup script -*- lexical-binding:t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>

;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Commentary:
;;

;;; Code:

(let* ((this-file (or load-file-name (buffer-file-name)))
       (this-dir (file-name-directory this-file)))
  ;; Local emacs configuration
  (load-file (concat this-dir "emacs.d/init.el"))
  (add-to-list 'load-path (expand-file-name "../elisp" this-dir)))

(require 'emacs-drawio)
(require 'emacs-drawio-global)
(require 'emacs-drawio-reload)
(require 'emacs-drawio-relaunch)
(require 'indium-chrome)

;; Set either in you user file to <path-to-chromium>
;; (indium only supports chromium derivatives). Examples:
;; - (setq indium-chrome-executable "chromium")
;; - (setq indium-chrome-executable
;;          "/path/to/draw....AppImage")
;; Note: even if VSCodium uses Electron, its DrawIO plugin uses
;; drawio's embedded mode: I don't know how it works and it's not
;; supported here.
;; Since I usually copy it in the cloned Quiversinapot-mxgraph dir,
;; that's the default (draw.io's AppImage is already in .gitignore).
(setq indium-chrome-executable
      ;; Wrapper script passes additional switches to app
      (expand-file-name "example-emacs-config/drawio_launcher_wrapper.bash"
                        emacs-drawio-global-dir))

(cl-defun run-emacs-drawio-inject-js-file
    (&optional (js-file-to-inject (expand-file-name
                                   "inject.js"
                                   emacs-drawio-global-user-dir)))
  "Load `JS-FILE-TO-INJECT' if it exists."
  (when (file-exists-p js-file-to-inject)
    (indiumeval-load-file js-file-to-inject)))

;; Reload emacs-drawio-inject1 every time `emacs-drawio-reload' and
;; `emacs-drawio-relaunch' finish.
(add-hook 'emacs-drawio-reload-post-reload-hook
          #'run-emacs-drawio-inject-js-file)

;; Compile js, run app, connect to it via Indium, inject js via Indium.
;; For some resaon, only at startup, the while loop inside
;; emacs-drawio-relaunch that waits for emacs-drawio-compile-watcher hangs Emacs
;; when run synchronously. Running `emacs-drawio-relaunch' with timer avoids
;; hanging at initialization.
(run-with-timer
 0.1 nil (lambda () (emacs-drawio-relaunch emacs-drawio-global-dir nil t)))

(provide 'run-emacs-drawio)
;;; run-emacs-drawio.el ends here
