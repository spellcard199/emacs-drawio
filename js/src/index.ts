/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

/// <reference path="../node_modules/mxgraph-type-definitions/index.d.ts" />

import { EmacsDrawioPlugin } from "./EmacsDrawioPlugin"
import { edrLoadCustom } from "./extra/eightDrovesOfRabbits"

const emacsDrawioPlugin = EmacsDrawioPlugin.loadPlugin()
edrLoadCustom(emacsDrawioPlugin)
