#!/bin/env bash
SCRIPTS_DIR="$(dirname ${BASH_SOURCE[0]})"
EMACS_DIR="$SCRIPTS_DIR/../emacs"
CUR_DIR="$(pwd)"

cd "$EMACS_DIR" && cask exec buttercup -L .

cd "$CUR_DIR"
