;;; emacs-drawio-relaunch.el --- Functions related to relaunching diagramming app

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;

;;; Code:

(require 'emacs-drawio-reload)
(require 'indiumeval-eval)

(defun emacs-drawio-refresh-page ()
  (indiumeval-eval-sync "window.location = window.location"))

(defun emacs-drawio-refresh--wait-and-click-on-close-dialog ()
  (indiumeval-eval-loop
   ;; Get "Create New Diagram" close button
   (concat
    "window['emacsDrawioTmpElemArr']"
    "  = Array.from(document.getElementsByClassName('geDialogClose'))"
    "    .filter(elt => elt.hasAttribute('title'));"
    "window['emacsDrawioTmpElemArr'];")
   (lambda (iro)
     (when iro
       (string-equal "Array(1)"
                     (indium-remote-object-to-string iro))))
   100 60)
   ;; Click on "Create New Diagram" close button
  (indiumeval-eval-sync
   "window['emacsDrawioTmpElemArr'][0].click(); delete window['emacsDrawioTmpElemArr']"
   nil t))

(defun emacs-drawio-refresh--close-dialog-inject()
  (emacs-drawio-refresh--wait-and-click-on-close-dialog)
  (emacs-drawio-reload--inject-js))

(defun emacs-drawio-refresh-reload()
  (interactive)
  (emacs-drawio-refresh-page)
  (emacs-drawio-refresh--wait-and-click-on-close-dialog)
  (emacs-drawio-reload))

(provide 'emacs-drawio-refresh)
;;; emacs-drawio-refresh.el ends here
