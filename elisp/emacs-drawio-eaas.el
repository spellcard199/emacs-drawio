;;; emacs-drawio-eaas.el --- Emacs functions triggered by and calling js side -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2023, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;
;; Emacs functionalities that the Diagrams.net app can
;; invoke.  Currently the way that the Diagrams.net app communicates
;; with Emacs is using `indium-client-log-hook'.
;;

(require 'indiumeval-call)
(require 'emacs-drawio-org)
(require 'emacs-drawio-data)
(require 'emacs-drawio-data-util)
(require 'org)
(require 'evil)
(require 'face-remap)
(require 'emacs-drawio-mode)

;;; Code:

(defvar emacs-drawio-eaas-edit-selection-cells--frame nil
  "Frame to be used when \"editSelectionCells\" is received.")

(defvar emacs-drawio-eaas-edit-selection-cells--buffer-name "*emacs-drawio*"
  "Name of the buffer where cells can be edited from Emacs.")

(defvar emacs-drawio-eaas-edit-selection-cells--save-mode-line-format nil)

(defvar emacs-drawio-eaas-selection-cells-autosync-limit 5000
  "Compared with `emacs-drawio-eaas-selection-cells--length' by autosync.")

(defvar emacs-drawio-eaas-selection-cells--length 0
  "Compared with `emacs-drawio-eaas-selection-cells-autosync-limit' by autosync.
Updated by messages from GraphModelListener and GraphSelectionModelListener.")

(defvar emacs-drawio-eaas-org--from-app-sync--in-progress nil)

(defvar emacs-drawio-current-diagram-path nil
  ;; Why this variable isn't buffer-local: we are still using Indium
  ;; to connect to Diagrams.net, so there can only be one diagram
  ;; being edited at a time.
  "Path of the last diagram loaded by the Diagrams.net app.
Updated when fileLoaded listener is triggered.")


(cl-defstruct (emacs-drawio-eaas-to-app-async-data-getter
               (:constructor emacs-drawio-eaas-to-app-async-data-getter--create)
               (:copier nil))
  (func
   nil
   :type function
   :documentation
   "Nullary function that returns a `emacs-drawio-data-diagram'.")
  (repeat-in-queue nil
                   :type boolean
                   :documentation
                   "REPEAT-IN-QUEUE tells merger function if the same function should
be called again if present multiple times in queue."))

(defun emacs-drawio-eaas-to-app-async-data-getter-create
    (func &optional repeat-in-queue)
  "Constructor for `emacs-drawio-eaas-to-app-async-data-getter'.
Arguments FUNC and REPEAT-IN-QUEUE are described in
`emacs-drawio-eaas-to-app-async-data-getter'."
  (cl-check-type func function)
  (cl-assert (null (help-function-arglist func)))
  (emacs-drawio-eaas-to-app-async-data-getter--create
   :func func
   :repeat-in-queue repeat-in-queue))

(defun emacs-drawio-eaas-to-app-async-data-getter-funcall
    (data-getter)
  "Funcall DATA-GETTER's func and check return type."
  (cl-check-type data-getter
                 emacs-drawio-eaas-to-app-async-data-getter)
  (let ((diagram-data
         (funcall (emacs-drawio-eaas-to-app-async-data-getter-func
                   data-getter))))
    ;; Check return type
    (cl-check-type diagram-data emacs-drawio-data-diagram)
    diagram-data))

(defvar emacs-drawio-eaas-org--to-app-async--in-queue nil
  "List of data-getters waiting to be called by syncing func.
Each data-getter takes no parameters and returns a
`emacs-drawio-data-diagram'.")

(defvar emacs-drawio-eaas-org--to-app-async--in-progress nil
  "List of data-getters already called by syncing in progress.
Each data-getter takes no parameters and returns a
`emacs-drawio-data-diagram'.")

(defvar emacs-drawio-eaas-changed-cells-watcher-enabled t
  "If nil to-app-async--after-change-func sends whole buffer.")

(defvar emacs-drawio-eaas-changed-cells--store '()
  "Used to reduce input lag during autosync.
The shape of the store is:
  ((\"Page-name\" . <hashmap:cell-id->org-string>)
   ...
   (\"Page-name\" . <hashmap:cell-id->org-string>)
  )")

(defun emacs-drawio-eaas-indium-client-log-hook-remove ()
  "Remove `emacs-drawio-eaas--app-listener' from indium log hook."
  (remove-hook 'indium-client-log-hook #'emacs-drawio-eaas--app-listener))

(defun emacs-drawio-eaas-indium-client-log-hook-add ()
  "Add `emacs-drawio-eaas--app-listener' to `indium-client-log-hook'."
  (when (indium-client-process-live-p)
    (let ((emacsDrawioToString
           (indium-remote-object-to-string
            (indiumeval-call-sync "emacsDrawio.toString"))))
      (if (string-equal "\"[object Object]\"" emacsDrawioToString)
          (add-hook 'indium-client-log-hook #'emacs-drawio-eaas--app-listener)
        (message
         (concat
          "[ERROR] `emacs-drawio-eaas--app-listener' hasn't been "
          "added to `indium-client-log-hook'.\n"
          "Reason: window.emacsDrawio.toString()\n"
          "- expected: \"[object Object]\"\n"
          "- actual:  %S")
         emacsDrawioToString)))))

(defmacro emacs-drawio-eaas-with-edit-selection-cells-buffer (&rest forms)
  "Ensure FORMS are evaluated in editing buffer with `org-mode'."
  `(with-current-buffer
       (get-buffer-create
        emacs-drawio-eaas-edit-selection-cells--buffer-name)
     (when (not (equal major-mode 'org-mode))
       (org-mode)
       (emacs-drawio-mode 1))
     ,@forms))

(cl-defun emacs-drawio-eaas-org--get-selection-cells-data
    (&optional (graph-set-enabled t))
  "Get selection cells data as an elisp structure.
Argument GRAPH-SET-ENABLED is converted to boolean and passed to
JS side."
  (let* ((selectionCellsStringified
          (substring-no-properties
           (indium-render-remote-object-to-string
            (indiumeval-call-sync
             "emacsDrawio.emacsManager.getSelectionCellsDataStringified"
             "emacsDrawio.emacsManager.editorUi"
             (if graph-set-enabled "true" "false"))))))
    (json-read-from-string
     ;; remove "\"" from both prefix and suffix: they are added by the
     ;; REPL but mess up with reading.
     (string-trim selectionCellsStringified "\"" "\""))))

(cl-defun emacs-drawio-eaas-org--sync-from-selection-cells
    (&optional (graph-set-enabled t))
  "Update contents of *emacs-drawio* buffer based on selection cells.
Argument GRAPH-SET-ENABLED is converted to boolean and passed to JS side."
  (interactive (list t))
  (unwind-protect
      (emacs-drawio-eaas-with-edit-selection-cells-buffer
       (setq emacs-drawio-eaas-org--from-app-sync--in-progress t)
       (let* ((diagram-data (emacs-drawio-eaas-org--get-selection-cells-data
                             graph-set-enabled))
              (org-string (emacs-drawio-org-from-ejson-doc diagram-data)))
         (with-silent-modifications
           ;; Since `with-silent-modifications' allows removing text
           ;; properties and writing on read-only text,
           ;; remove-text-properties is unneeded:
           ;;   (remove-text-properties (point-min) (point-max) '(read-only t))
           (insert (prog1 org-string (erase-buffer)))
           )
         (outline-show-all)
         (goto-char (point-min))
         (save-match-data
           (search-forward "label\n\n" nil t))))
    (setq emacs-drawio-eaas-org--from-app-sync--in-progress nil)
    (setq emacs-drawio-eaas-changed-cells--store nil)))

(defun emacs-drawio-eaas-org--data-getter-for-buffer ()
  "Get data for entire default buffer."
  (with-current-buffer emacs-drawio-eaas-edit-selection-cells--buffer-name
    (emacs-drawio-org-parse-org-doc (buffer-substring-no-properties
                                     (point-min) (point-max)))))

(defun emacs-drawio-eaas-org--to-app-async--merge-diagram-data-getters
    (list-of-data-getters)
  "Filter, call and merge results of LIST-OF-DATA-GETTERS."
  (let (;; - delete-dups: avoid calling the same function multiple
        ;;   times.
        ;; - Doubled reverse: if we used just delete-dups the least
        ;;   recent one would remain, while we want the most recent
        ;;   one.
        (filtered-getters
         (reverse (delete-dups (reverse list-of-data-getters))))
        (already-called-funcs nil)
        (merged-diagram-data (emacs-drawio-data-diagram-create)))
    (dolist (getter filtered-getters)
      ;; Update merged-diagram-data with result of data-getter.  When
      ;; data-getter's repeat-in-queue is nil, skip already called
      ;; function.
      (let*
          ((f (emacs-drawio-eaas-to-app-async-data-getter-func
               getter))
           (do-funcall
            (or (not (member f already-called-funcs))
                (emacs-drawio-eaas-to-app-async-data-getter-repeat-in-queue
                 getter)))
           (diagram-data
            (when do-funcall
              (push f already-called-funcs)
              (emacs-drawio-eaas-to-app-async-data-getter-funcall
               getter)))
           (merged-pages-alist
            (when diagram-data
              (mapcar #'emacs-drawio-data-util-page-to-cons
                      (emacs-drawio-data-diagram-pages
                       merged-diagram-data))))
           (pages
            (when diagram-data
              (emacs-drawio-data-diagram-pages
               diagram-data))))
        (dolist (page-data pages)
          ;; Data needed once per page
          (let* ((pagename-cells-cons
                  (emacs-drawio-data-util-page-to-cons
                   page-data))
                 (page-name (car pagename-cells-cons))
                 (cells-ht (cdr pagename-cells-cons))
                 (merged-pagename-cells-cons
                  (assoc page-name merged-pages-alist))
                 (merged-cells-ht (cdr merged-pagename-cells-cons)))
            ;; If page already exists in merged data, update cells
            (if merged-pagename-cells-cons
                (maphash
                 (lambda (cell-id cell-data)
                   (remhash cell-id merged-cells-ht)
                   (puthash cell-id cell-data merged-cells-ht))
                 cells-ht)
              ;; If page does not exist in merged data, add it
              (emacs-drawio-data-util-diagram-add-page
               merged-diagram-data
               page-data))))))
    merged-diagram-data))

(defun emacs-drawio-eaas-org--to-app-async--queue-run--after-sync
    (&optional _iro success-func error-func)
  "Helper for `emacs-drawio-eaas-org--to-app-async--queue-run'.
Argument IRO is the result of `indium-eval'.
Arguments SUCCESS-FUNC anc ERROR-FUNC are described in
`emacs-drawio-eaas-org--to-app-async--queue-run'."
  (when t ;; TODO: check for error in js output
    (setq emacs-drawio-eaas-org--to-app-async--in-progress nil)
    (if emacs-drawio-eaas-org--to-app-async--in-queue
        (let ((next-to-sync emacs-drawio-eaas-org--to-app-async--in-queue))
          (setq emacs-drawio-eaas-org--to-app-async--in-queue nil)
          (emacs-drawio-eaas-org--to-app-async--queue-run
           next-to-sync success-func error-func)))))

(defun emacs-drawio-eaas-org--to-app-async--queue-run
    (data-getter-funcs
     &optional
     ;; TDOO: think about signature, where to call success-func and
     ;; error-func
     success-func
     error-func)
  "Update cells based on `DATA-GETTER-FUNC'.
Argument DATA-GETTER-FUNCS can be:
- a nullary functions that returns a struct of type
  `emacs-drawio-data-diagram',
- an `emacs-drawio-eaas-to-app-async-data-getter'
- a list of the former types.
An `emacs-drawio-eaas-to-app-async-data-getter' allows to tell the
merging strategy, which runs before syncing, to not skip calling the
same function when present multiple times in queue.
Called interactively, if the name of current buffer is equal to
`emacs-drawio-eaas-edit-selection-cells--buffer-name', uses a default
`DATA-GETTER-FUNC' that extracts data from there.  Otherwise return
nil.
The reason this function takes a DATA-GETTER-FUNC instead of just an
org-string (or already parsed data) is:
1. For large buffers (buffer-substring-no-content (point-min)
   (point-max)) is the slowest step
2. To avoid unnecessary input lag in case case org-string changes
   before the previous sync is completed: we want to compute just the
   last value.
TODO: Arguments SUCCESS-FUNC and ERROR-FUNC are unused yet."
  (interactive (when (string-equal emacs-drawio-eaas-edit-selection-cells--buffer-name
                                   (buffer-name (current-buffer)))
                 (list #'emacs-drawio-eaas-org--data-getter-for-buffer)))
  ;; TODO/NOTE: If either...:
  ;; - `emacs-drawio-eaas-org--to-app-async--in-queue' contains a
  ;;   func that depends on a global variable that changes
  ;; - a successive change occurs that replaces the old
  ;;   `emacs-drawio-eaas-org--to-app-async--in-queue'
  ;; ... changes can remain unsynced.
  ;; Think of a safer way. E.g. maybe:
  ;; - this function could accept another variable to specify if
  ;;   data-getter-function is skippable or not
  ;; - `emacs-drawio-eaas-org--to-app-async--in-queue' could store for
  ;;   each function if it's skippable or not
  (when data-getter-funcs ;; TODO: generalize condition
    ;; If a syncing is in progress do not start another one, just set
    ;; a new value for item in queue: when current syncing is done the
    ;; callback passed to indium will take care of syncing the value
    ;; of `emacs-drawio-eaas-org--to-app-async--in-queue'. This way we
    ;; can avoid unnecessary intermediate syncings if this function is
    ;; called again and again before the one in progress finishes.
    (setq emacs-drawio-eaas-org--to-app-async--in-queue
          (append emacs-drawio-eaas-org--to-app-async--in-queue
                  ;; Convert all accepted types for
                  ;; `data-getter-funcs' to list of
                  ;; `emacs-drawio-eaas-to-app-async-data-getter'.
                  (mapcar
                   (lambda (func-or-data-getter)
                     (cond ((functionp func-or-data-getter)
                            (emacs-drawio-eaas-to-app-async-data-getter-create
                             func-or-data-getter))
                           ((emacs-drawio-eaas-to-app-async-data-getter-p
                             func-or-data-getter)
                            func-or-data-getter)))
                   (if (listp data-getter-funcs)
                       data-getter-funcs
                     (list data-getter-funcs)))))
    (unless emacs-drawio-eaas-org--to-app-async--in-progress
      (setq emacs-drawio-eaas-org--to-app-async--in-progress
            emacs-drawio-eaas-org--to-app-async--in-queue
            emacs-drawio-eaas-org--to-app-async--in-queue
            nil)
      (run-at-time
       nil nil
       (lambda ()
         (if-let* ((diagram-data
                    (condition-case err
                        ;; (funcall data-getter-func)
                        (emacs-drawio-eaas-org--to-app-async--merge-diagram-data-getters
                         emacs-drawio-eaas-org--to-app-async--in-progress)
                      (error (print err) nil)))
                   (encoded-data
                    (condition-case err
                        (emacs-drawio-data-util-diagram-json-encode
                         diagram-data)
                      (error (print err) nil))))
             (indiumeval-call
              "emacsDrawio.emacsManager.updatePagesStartEndEaasTask"
              nil
              (lambda (iro)
                (emacs-drawio-eaas-org--to-app-async--queue-run--after-sync
                 iro success-func error-func))
              "emacsDrawio.emacsManager.editorUi"
              encoded-data)
           ;; If an error occurs while parsing and encoding data,
           ;; don't leave
           ;; `emacs-drawio-eaas-org--to-app-async--in-progress'
           ;; stuck to non-nil.
           (emacs-drawio-eaas-org--to-app-async--queue-run--after-sync
            nil success-func error-func)))))))

(defun emacs-drawio-eaas-edit-selection-cells--frame-hide-decorations ()
  "Hide decorations."
  (when-let* ((frame emacs-drawio-eaas-edit-selection-cells--frame)
              (frame-live (frame-live-p
                           emacs-drawio-eaas-edit-selection-cells--frame)))
    (with-selected-frame frame
      (set-frame-parameter frame 'undecorated t)
      (setq mode-line-format nil))))

(defun emacs-drawio-eaas-edit-selection-cells--frame-show-decorations ()
  "Show decorations."
  (when-let* ((frame emacs-drawio-eaas-edit-selection-cells--frame)
              (frame-live (frame-live-p
                           emacs-drawio-eaas-edit-selection-cells--frame)))
    (with-selected-frame frame
      (set-frame-parameter frame 'undecorated nil)
      (setq mode-line-format
            emacs-drawio-eaas-edit-selection-cells--save-mode-line-format))))

(defun emacs-drawio-eaas-edit-selection-cells-frame-toggle-decorations ()
  "Toggle decorations."
  (let ((frame (emacs-drawio-eaas-edit-selection-cells--get-frame-create)))
    (if (frame-parameter frame 'undecorated)
        (emacs-drawio-eaas-edit-selection-cells--frame-show-decorations)
      (emacs-drawio-eaas-edit-selection-cells--frame-hide-decorations))))

(defun emacs-drawio-eaas--wayland-p ()
  (string-equal (getenv "XDG_SESSION_TYPE") "wayland"))

(defun emacs-drawio-eaas--x11-p ()
  (string-equal (getenv "XDG_SESSION_TYPE") "x11"))

(defun emacs-drawio-eaas-edit-selection-cells--get-frame-create ()
  "Get `emacs-drawio-eaas-edit-selection-cells--frame'.
Create it if does not already exists."
  (unless (frame-live-p emacs-drawio-eaas-edit-selection-cells--frame)
    (setq emacs-drawio-eaas-edit-selection-cells--frame
          (let* (;; (monitor-geom (frame-monitor-attribute 'geometry))
                 ;; (monitor-width (nth 2 monitor-geom))
                 ;; (monitor-height (nth 3 monitor-geom))
                 (frame (make-frame
                         `((user-position . t)
                           (width . 40)
                           (height . 15)
                           (top . 0)
                           ;; [[info:elisp#Position Parameters]]
                           (left . (- 0))
                           (horizontal-scroll-bars . nil)
                           (vertical-scroll-bars . nil)
                           (visibility
                            ;; Workaround for wayland: if frame is
                            ;; created invisible it glitches and can't
                            ;; be made visible.
                            . ,(cond ((emacs-drawio-eaas--wayland-p)
                                      t)
                                     (t nil)))
                           ))))
            (with-selected-frame frame
              (setq
               emacs-drawio-eaas-edit-selection-cells--save-mode-line-format
               mode-line-format))
            frame)))
  emacs-drawio-eaas-edit-selection-cells--frame)

(defun emacs-drawio-eaas-edit-selection-cells-done-editing ()
  "Sync buffer to app and hide frame."
  (interactive)
  (emacs-drawio-eaas-with-edit-selection-cells-buffer
   (emacs-drawio-eaas-org--to-app-async--queue-run
    #'emacs-drawio-eaas-org--data-getter-for-buffer))
  (make-frame-invisible emacs-drawio-eaas-edit-selection-cells--frame t))

(defun emacs-drawio-eaas-get-current-diagram-path ()
  "Get current diagram path via indium."
  (let* ((js-string-quoted
          (indium-remote-object-to-string
           (indiumeval-call-sync
            "emacsDrawio.emacsManager.getCurrentFileUrlOrPath")))
         (js-string-dequoted
          (string-trim-right
           (string-trim-left js-string-quoted "\"") "\"")))
    js-string-dequoted))

(defun emacs-drawio-eaas-get-current-diagram-path-set-buffer-var ()
  "Get current diagram path and set relative elisp var.
`emacs-drawio-current-diagram-path' is set in the buffer specified by
`emacs-drawio-eaas-edit-selection-cells--buffer-name'."
  (let ((diagram-path (emacs-drawio-eaas-get-current-diagram-path)))
    (emacs-drawio-eaas-with-edit-selection-cells-buffer
     (setq emacs-drawio-current-diagram-path diagram-path))))

(defun emacs-drawio-eaas-handle--edit-selection-cells (_data)
  "Get selectionCells, translate to org, insert in dedicated buffer."
  (let* ((frame (emacs-drawio-eaas-edit-selection-cells--get-frame-create))
         ;; (frame-width (frame-width frame))
         ;; (frame-height (frame-height frame))
         ;; Used by workarounds for GNU/Linux:
         (_ (set-frame-parameter frame 'user-position t))
         (frame-pos (frame-position frame))
         (frame-left (car frame-pos))
         (frame-top (cdr frame-pos)))
    ;; Workaround for GNU/Linux in XOrg session: if the frame is
    ;; already visibile it remains in background even after
    ;; `select-frame-set-input-focus'.  However the same workaround is
    ;; in wayland is not needed and causes a glitch where the window
    ;; remains invisible.
    (cond ((string-equal (getenv "XDG_SESSION_TYPE") "x11")
           (when (frame-visible-p frame)
             (make-frame-invisible frame t)
             (make-frame-visible frame)))
          (t
           (make-frame-visible frame)))
    ;; Workaround for GNU/Linux: when frame becomes visible there is a
    ;; small displacement on the x axis. Reposition frame where it was
    ;; before the visible-invisible dance.
    (let ((delta-x-fix (- frame-left
                          (car (frame-position frame)))))
      (set-frame-position frame (+ delta-x-fix frame-left) frame-top))
    (select-frame-set-input-focus frame)
    ;; Another workaround, this time for Emacs 28. Without this frame
    ;; remains behind other windows.
    ;; TODO: delete this when a better solution is found out.
    (shell-command-to-string
     "xdotool search --onlyvisible \"[*]emacs-drawio[*]\" windowactivate")
    (with-selected-frame frame
      (switch-to-buffer (get-buffer-create
                         emacs-drawio-eaas-edit-selection-cells--buffer-name)))))

(defmacro emacs-drawio-eaas--with-after-change-func-disabled (form)
  "Eval FORM with `emacs-drawio-eaas-org--buffer-watcher-enabled' as nil.
Then reset `emacs-drawio-eaas-org--buffer-watcher-enabled' to its previous
value."
  `(let ((is-enabled (emacs-drawio-eaas-to-app-async-autosync-enabled-p)))
     (unwind-protect
         (progn
           (emacs-drawio-eaas-org--after-change-func--remove-hook)
           ,form)
       (when is-enabled
         (emacs-drawio-eaas-org--after-change-func--add-hook)))))

(defun emacs-drawio-eaas-handle--graph-changed (data)
  "Handle DATA passed by GraphModelListener."
  ;; TODO : handle case where emacs buffer changed and a change to
  ;; graph is made from diagrams.net app (The way I use it how it
  ;; shouldn't happen because of the time check in
  ;; `emacs-drawio-eaas-org--buffer-watcher-enabled', but in future
  ;; the hole could emerge back.
  (emacs-drawio-eaas--with-after-change-func-disabled
   (let ((eaas-task-in-progress (cdr (assoc 'eaasTaskInProgress data)))
         (selection-cells-length (cdr (assoc 'selectionCellsLength data))))
     (setq emacs-drawio-eaas-selection-cells--length selection-cells-length)
     ;; "eaasGraphChanged"
     ;; - can be triggered by actions from Emacs
     ;; - can in turn trigger actions in Emacs
     ;; To avoid unnecessary syncings and degraded typing
     ;; fluency when the emacs buffer is modified, we avoid
     ;; syncing from diagrams.net if the first event probably
     ;; originated from emacs.
     ;; Update: after change in code for in GraphModelListener and
     ;; GraphSelectionModelListener:
     ;; 1. They don't produce a log if eaasTaskInProgress is true.
     ;; 2. This check is now redundant: keeping it to remember
     ;;    where it was if typescript side is reverted in future.
     (when (equal :json-false eaas-task-in-progress)
       (emacs-drawio-eaas-org--sync-from-selection-cells t)))))

(defun emacs-drawio-eaas-handle--selection-changed (data)
  "Handle DATA passed by GraphSelectionModelListener."
  ;; TODO: read TODO in `emacs-drawio-eaas-handle--graph-changed'.
  (emacs-drawio-eaas--with-after-change-func-disabled
   (let ((selection-cells-length
          (cdr (assoc 'selectionCellsLength data))))
     (setq emacs-drawio-eaas-selection-cells--length selection-cells-length)
     (emacs-drawio-eaas-org--sync-from-selection-cells t))))

(defun emacs-drawio-eaas-handle--file-loaded (_data)
  "Handle fileLoaded event."
  (let* ((js-string-quoted
          (indium-remote-object-to-string
           (indiumeval-call-sync
            "emacsDrawio.emacsManager.getCurrentFileUrlOrPath")))
         (js-string-dequoted
          (string-trim-right
           (string-trim-left js-string-quoted "\"") "\"")))
    (emacs-drawio-eaas-with-edit-selection-cells-buffer
     (setq emacs-drawio-current-diagram-path
           js-string-dequoted))))

(defun emacs-drawio-eaas--app-listener-helper
    (message &optional _error)
  "Listens and dispatch messages logged by diagram.net app.
When added to `indium-client-log-hook', detect when if some known
string is in the message and dispatch associated data to handler.
Arguments MESSAGE and ERROR are passed by indium when this function
is added to `indium-client-log-hook'."
  (if-let* ((message-type (cdr (assoc 'type message)))
            (message-iro (cdr (assoc 'result message)))
            (iro-string-with-quotes (indium-remote-object-to-string message-iro))
            ;; If eaas-msg-p is nil message should not be handled: if-let* exits
            (eaas-msg-p (string-prefix-p "\"//eaas  " iro-string-with-quotes))
            (eaas-data-str (string-trim iro-string-with-quotes "\"//eaas  " "\""))
            (eaas-data (json-read-from-string eaas-data-str))
            (eaas-verb (cdr (assoc 'verb eaas-data))))
      (when (and (not emacs-drawio-eaas-org--to-app-async--in-progress)
                 (string-equal "log" message-type))
        (cond ((string-equal "eaasEditSelectionCells" eaas-verb)
               (emacs-drawio-eaas-handle--edit-selection-cells eaas-data))
              ((string-equal "eaasGraphChanged" eaas-verb)
               (emacs-drawio-eaas-handle--graph-changed eaas-data))
              ((string-equal "eaasSelectionChanged" eaas-verb)
               (emacs-drawio-eaas-handle--selection-changed eaas-data))
              ((string-equal "eaasFileLoaded" eaas-verb)
               (emacs-drawio-eaas-handle--file-loaded eaas-data)
               )
              ))))

(defun emacs-drawio-eaas--app-listener
    (message &optional error)
  "Pass args to `emacs-drawio-eaas--app-listener-helper'.
Argument MESSAGE: see `emacs-drawio-eaas--app-listener-helper'.
Argument ERROR: see `emacs-drawio-eaas--app-listener-helper'."
  ;; WARNING: if you change this function remember to remove and
  ;; re-add it to `indium-client-log-hook' or changes won't be applied
  ;; (even if you use eval-defun). Use helper for faster iterations
  ;; during development.
  (emacs-drawio-eaas--app-listener-helper message error))

;; Autosync Emacs->Diagrams.net

(defun emacs-drawio-eaas-org--after-change-func--data-getter-func ()
  "Used by `emacs-drawio-eaas-org--after-change-func'."
  ;; A possible optimization could have been to port conversion
  ;; from/to org <-> json to JavaScript side.  However, since Emacs 28
  ;; with nativecomp, json-encode is not a bottleneck anymore (if you
  ;; avoid encoding hash-tables), so the remaining ones are:
  ;; - parsing org cells
  ;; - sending data via Indium
  ;; Sending only changed cells we can improve both, since:
  ;; - less cells to parse
  ;; - less data to send

  ;; TODO: `emacs-drawio-eaas-changed-cells-watcher-enabled' changing
  ;; to non-nil when a full buffer sync is in
  ;; `emacs-drawio-eaas-org--to-app-async--in-queue' is an example of
  ;; when `emacs-drawio-eaas-org--to-app-async--queue-run' may cause
  ;; loss of sync. Read TODO there.
  (prog1 (if emacs-drawio-eaas-changed-cells-watcher-enabled
             (emacs-drawio-eaas-changed-cells--to-data)
           (emacs-drawio-eaas-org--data-getter-for-buffer))
    (setq emacs-drawio-eaas-changed-cells--store nil)))

(defun emacs-drawio-eaas-org--after-change-func
    (beg end prev-len)
  "If conditions are true, sync dedicated org buffer to app.
Arguments BEG, END and PREV-LEN are passed when the
`after-change-functions' hook is run."
  (when-let*
      ;; Slower checks should go after faster ones.
      ((ok (<= emacs-drawio-eaas-selection-cells--length
               emacs-drawio-eaas-selection-cells-autosync-limit))
       ;; Avoid loop: sync to app -> sync from app -> ...
       (ok (not emacs-drawio-eaas-org--from-app-sync--in-progress))
       (buffer (current-buffer))
       (buffer-name (buffer-name buffer))
       (ok (string-equal emacs-drawio-eaas-edit-selection-cells--buffer-name
                         buffer-name))
       (ok (emacs-drawio-eaas-to-app-async-autosync-enabled-p)))
    ;; Update `emacs-drawio-eaas--changed-cells--store'
    (when emacs-drawio-eaas-changed-cells-watcher-enabled
      (emacs-drawio-eaas-changed-cells--update-changed--after-change-function
       beg end prev-len))
    (emacs-drawio-eaas-org--to-app-async--queue-run
     ;; data-getter-func
     #'emacs-drawio-eaas-org--after-change-func--data-getter-func
     ;; success-func
     nil
     ;; error-func
     ;; TODO: If sync fails: re-compute
     ;; `emacs-drawio-eaas-changed-cells--store' for each cell-id in data
     ;; Note that this implies
     ;; `emacs-drawio-eaas-org--to-app-async--queue-run' passing data
     ;; to error-func.
     nil)))

(defun emacs-drawio-eaas-org--after-change-func--remove-hook ()
  "Remove hook from `after-change-functions'."
  (emacs-drawio-eaas-with-edit-selection-cells-buffer
   (remove-hook 'after-change-functions
                'emacs-drawio-eaas-org--after-change-func
                t)))

(defun emacs-drawio-eaas-org--after-change-func--add-hook ()
  "Add hook to `after-change-functions'."
  (emacs-drawio-eaas-with-edit-selection-cells-buffer
   (emacs-drawio-eaas-org--after-change-func--remove-hook)
   (add-hook 'after-change-functions
             'emacs-drawio-eaas-org--after-change-func
             nil t)))

;;; Track cells changed in emacs buffer

(defun emacs-drawio-eaas-changed-cells--to-data ()
  "Convert changed cells store to `emacs-drawio-data-diagram'."
  (emacs-drawio-data-diagram-create
   :pages (mapcar
           (lambda (cons-page-cells)
             (emacs-drawio-data-page-create
              :page-name (car cons-page-cells)
              :cells-ht (cdr cons-page-cells)))
           emacs-drawio-eaas-changed-cells--store)))

(defun emacs-drawio-eaas-changed-cells--update-changed
    (page-name cell-id cell-beg cell-end)
  "Add CELL-ID with org-string, PAGE-NAME to store.
Arguments CELL-BEG and CELL-END are used to find the org-string for
CELL-ID."
  (setq debug-on-error t)
  (let* ((page-assoc-maybe
          (assoc page-name emacs-drawio-eaas-changed-cells--store))
         (cell-ids-ht
          (if page-assoc-maybe
              (or (cdr page-assoc-maybe)
                  (setf (cdr page-assoc-maybe)
                        (make-hash-table :test #'equal)))
            (let ((ht (make-hash-table :test #'equal)))
              (push (cons page-name ht)
                    emacs-drawio-eaas-changed-cells--store)
              ;; push returns PLACE, not NEWELT
              ht))))
    (remhash cell-id cell-ids-ht) ;; don't want duplicates
    (puthash cell-id
             (buffer-substring-no-properties cell-beg cell-end)
             cell-ids-ht)))

(defun emacs-drawio-eaas-changed-cells--update-changed--after-change-function
    (beg end _prev-len)
  "Add cell-ids involved by change to store.
Argument BEG is where changed region begins.
Argument END is where changed region ends."
  (save-match-data
    ;; The first function we are calling that changes `match-data' is
    ;; `org-current-level'.
    (when (and (string-equal
                (buffer-name)
                emacs-drawio-eaas-edit-selection-cells--buffer-name)
               (< 3 (or (org-current-level) 0)))
      (let (cur-page-re-point
            cur-page-name
            cur-cell-id
            cur-cell-beg
            cur-cell-end)
        (save-excursion
          (goto-char beg)
          ;; Should not raise error because (org-current-level) is > 3
          (re-search-backward "^[*][*][*][ ]")
          ;; Get first page-name
          (setq cur-page-re-point (point))
          (setq cur-page-name
                (string-trim (substring-no-properties
                              (thing-at-point 'line))
                             "*** "))
          ;; Skip cells in page but before change
          (goto-char beg)
          ;; Get cell-beg, cell-id, for cell at beg of change.
          (when (re-search-backward "^[*][*][*][*][ ]" cur-page-re-point t)
            ;; (setq cur-cell-beg (- (point) 5))
            (setq cur-cell-beg (point))
            (setq cur-cell-id (string-trim (substring-no-properties
                                            (thing-at-point 'line))
                                           "**** "))
            ;; Skip current cell in successive searches
            (forward-char 1))
          ;; Get successive pages and cell ids
          (while (re-search-forward
                  "\\(^[*][*][*][ ]\\|^[*][*][*][*][ ]\\)"
                  end t )
            (let* ((p (point))
                   (lb-limit (if (> p 3) (- p 4) 0)))
              (if (looking-back "^[*][*][*][ ]" lb-limit)
                  ;; Page heading
                  (progn
                    ;; Store data of previous cell
                    (setq cur-cell-end (- p 4))
                    (emacs-drawio-eaas-changed-cells--update-changed
                     cur-page-name cur-cell-id cur-cell-beg cur-cell-end)
                    ;; Reset data
                    (setq cur-cell-id nil)
                    (setq cur-cell-beg nil)
                    (setq cur-cell-end nil)
                    ;; Move to next page
                    (setq cur-page-re-point (point))
                    (setq cur-page-name
                          (string-trim (substring-no-properties
                                        (thing-at-point 'line))
                                       "*** ")))
                ;; Store data of previous cell
                (setq cur-cell-end (- p 5))
                (emacs-drawio-eaas-changed-cells--update-changed
                 cur-page-name cur-cell-id cur-cell-beg cur-cell-end)
                ;; Reset data
                (setq cur-cell-id nil)
                (setq cur-cell-beg nil)
                (setq cur-cell-end nil)
                ;; Move to next cell
                (setq cur-cell-id (string-trim (substring-no-properties
                                                (thing-at-point 'line))
                                               "**** ")))))
          ;; If at the end of change there is a cell, get its cell-end
          ;; and store data.
          (when cur-cell-beg
            (if-let* ((point-max (point-max))
                      (next-page-or-cell
                       (re-search-forward
                        "\\(^[*][*][*][ ]\\|^[*][*][*][*][ ]\\)"
                        point-max t))
                      (p (point))
                      (cell-end (if (looking-back "^[*][*][*][ ]" (- p 4))
                                    ;; After cell comes a page
                                    (- p 4)
                                  ;; After cell comes a cell
                                  (- p 5))))
                ;; cell-end is at next page or cell
                (setq cur-cell-end cell-end)
              ;; cell-end is at end of buffer
              (setq cur-cell-end point-max))
            (emacs-drawio-eaas-changed-cells--update-changed
             cur-page-name cur-cell-id cur-cell-beg cur-cell-end)))))))

(defun emacs-drawio-eaas-to-app-async-autosync-enabled-p ()
  "Return non-nil if Emacs->Diagrams.net autosync is enabled."
  (when-let ((b (get-buffer emacs-drawio-eaas-edit-selection-cells--buffer-name)))
    (with-current-buffer b
      (member 'emacs-drawio-eaas-org--after-change-func
              after-change-functions))))

(defun emacs-drawio-eaas-to-app-async-autosync-toggle ()
  "Toggle autosyncing in `emacs-drawio-compile-watcher-buffer-name'."
  (interactive)
  (let ((is-enabled (emacs-drawio-eaas-to-app-async-autosync-enabled-p)))
    (if is-enabled
        (emacs-drawio-eaas-org--after-change-func--remove-hook)
      (emacs-drawio-eaas-org--after-change-func--add-hook))
    (message "Emacs->Diagrams.net autosync enabled: %s"
             (if (emacs-drawio-eaas-to-app-async-autosync-enabled-p) t nil))))

(defun emacs-drawio-eaas-save-file ()
  "Save current diagram."
  (interactive)
  (indiumeval-call
   "emacsDrawio.emacsManager.editorUi.saveFile"
   nil
   (lambda (iro)
     (message "%s %s" "Maybe saving diagram."
              (indium-remote-object-to-string iro)))
   "false"))

(provide 'emacs-drawio-eaas)
;;; emacs-drawio-eaas.el ends here
