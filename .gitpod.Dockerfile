FROM gitpod/workspace-full

# Install custom tools, runtime, etc.
RUN sudo apt-get update
RUN sudo apt-get install -y emacs

RUN mkdir -p "/home/gitpod/.local/bin"
ENV PATH "$PATH:/home/gitpod/.local/bin"
# Double make install is a workaround.
RUN (git clone "https://github.com/cask/cask" && cd cask && make install || make install)
