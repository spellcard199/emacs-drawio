;;; emacs-drawio.el --- Emacs + diagrams.net via Indium -*- lexical-binding:t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>

;; Author: spellcard199 <spellcard199@protonmail.com>
;; Maintainer: spellcard199 <spellcard199@protonmail.com>
;; Keywords: diagrams.net, mxgraph, diagrams,
;; Homepage: https://gitlab.com/spellcard199/geiser-kawa
;; Package-Requires: ((emacs "26.1") (indiumeval "0.0.1"))
;; SPDX-License-Identifier: GPL-3.0-or-later
;; Version: 0.0.1

;; This file is NOT part of GNU Emacs.

;;; Commentary:
;; A package to integrate Emacs into my workflow for making diagrams
;; with the diagrams.net app

(require 'emacs-drawio-eaas)
(require 'emacs-drawio-mode-keybinding)
(require 'emacs-drawio-relaunch)

;;; Code:

(provide 'emacs-drawio)
;;; emacs-drawio.el ends here
