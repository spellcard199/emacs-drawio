/**
 * Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { EditorUiCustomListener } from "./editorui/EditorUiCustomListener";
import * as util from "../util";
import { EditorFileLoadedListener } from "./editor";

type EaasVerb = "eaasEditSelectionCells" | "eaasGraphChanged" | "eaasSelectionChanged" | "eaasFileLoaded";
type EaasData = {
    verb: EaasVerb;
    eaasTaskInProgress: boolean;
    selectionCellsLength: number;
}

export class EmacsManager {

    editorUi: any;
    editorUiCustomListener = new EditorUiCustomListener(this)
    editorFileLoadedListener = new EditorFileLoadedListener(this)
    private enabled: boolean = false;

    private eaasTaskInProgress: boolean = false;

    private constructor(editorUi: any) {
        this.editorUi = editorUi;
    }

    static new(editorUi: any) {
        return new EmacsManager(editorUi);
    }

    isEnabled() {
        return this.enabled;
    }

    private setEnabled(boolean: boolean) {
        this.enabled = boolean;
    }

    public isEaasTaskInProgress() {
        return this.eaasTaskInProgress;
    };

    enable() {
        this.editorUiCustomListener.enable();
        this.editorFileLoadedListener.enable();
        this.setEnabled(true);
        console.log("[EmacsManager.enable] Done.")
    }

    disable() {
        this.editorUiCustomListener.disable();
        this.editorFileLoadedListener.disable();
        this.setEnabled(false);
        console.log("[EmacsManager.disable] Done.")
    }

    install() {
        this.editorUiCustomListener.install();
        this.editorFileLoadedListener.install();
    }

    uninstall() {
        this.disable();
        this.editorUiCustomListener.uninstall();
        this.editorFileLoadedListener.uninstall();
    }

    quit() {
        this.uninstall();
        console.log("[EmacsManager.quit] Done.");
    }

    // Functions for eaas - Emacs --> diagrams.net

    public getRawXmlFileDataStr = util.getRawXmlFileDataStr;

    public getSelectionCellsDataStringified = util.getSelectionCellsDataStringified;

    public startEaasTask() {
        this.eaasTaskInProgress = true;
        const graph: mxGraph = this.editorUi.editor.graph;
        graph.setEnabled(false);
    }

    public endEaasTask() {
        this.eaasTaskInProgress = false;
        const graph: mxGraph = this.editorUi.editor.graph;
        graph.setEnabled(true);
    }

    public updatePagesStartEndEaasTask(editorUi: any, pagesData: util.PagesData) {
        try {
            this.startEaasTask();
            util.updatePages(editorUi, pagesData);
        } finally {
            this.endEaasTask();
        }
    }

    // Functions for eaas - diagrams.net --> Emacs

    public editSelectionCells() {
        this.eaasMkEaasDataConsoleLog("eaasEditSelectionCells");
    }

    public getCurrentFileUrlOrPath(): string {
        return util.getEditorFileUrlOrPath(this.editorUi)
    }

    // Functions for eaas - diagrams.net --> Emacs (utils)

    public eaasMkEaasDataConsoleLog(verb: EaasVerb) {
        const eaasData: EaasData = this.eaasMkEaasData(verb);
        EmacsManager.eaasConsoleLog(eaasData);
    }

    private eaasMkEaasData(verb: EaasVerb): EaasData {
        const emacsManager = this;
        const graph: mxGraph = emacsManager.editorUi.editor.graph;
        return {
            verb: verb,
            eaasTaskInProgress: emacsManager.isEaasTaskInProgress(),
            selectionCellsLength: graph.getSelectionCells().length,
        }
    }

    private static eaasConsoleLog(eaasData: EaasData) {
        const msg = JSON.stringify(eaasData);
        console.log("//eaas  " + `${msg}`)
    }

}
