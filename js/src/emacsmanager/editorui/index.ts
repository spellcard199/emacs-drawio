/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { EditorUiCustomListener } from "./EditorUiCustomListener"

export { EditorUiCustomListener }
