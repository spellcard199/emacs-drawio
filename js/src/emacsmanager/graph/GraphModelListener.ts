/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { EmacsGraphManager } from "./EmacsGraphManager"

export class GraphModelListener {

    private enabled = false;
    emacsGraphManager: EmacsGraphManager

    constructor(emacsGraphManager: EmacsGraphManager) {
        this.emacsGraphManager = emacsGraphManager;
    }

    enable() {
        this.enabled = true;
    }

    disable() {
        this.enabled = false;
    }

    isEnabled() {
        return this.enabled;
    }

    apply(model: mxGraphModel, evt: mxEventObject) {
        if (this.isEnabled()) {
            const emacsManager = this.emacsGraphManager.euiCustomListener.emacsManager;
            if (!emacsManager.isEaasTaskInProgress()) {
                emacsManager.eaasMkEaasDataConsoleLog("eaasGraphChanged");
            }
        }
    }

}
