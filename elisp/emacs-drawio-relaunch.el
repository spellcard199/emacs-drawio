;;; emacs-drawio-relaunch.el --- Functions related to relaunching diagramming app -*- lexical-binding: t -*-

;; Copyright (C) 2020-2023, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;

;;; Code:

(require 'cl-lib)

(require 'emacs-drawio-global)
(require 'emacs-drawio-util)
(require 'emacs-drawio-compile)
(require 'emacs-drawio-reload)
(require 'emacs-drawio-refresh)
(require 'indium)

(defvar emacs-drawio-drawio-executable nil
  "Path to an executable launched by emacs-drawio.
Used as a temporary value for `indium-chrome-executable' while
calling `indium-launch'.")

(defvar emacs-drawio-relaunch-chrome-data-dir
  nil
  "Value passed to chrome's --user-data-dir by Indium.

NOTE: Indium connects to the electron app via the passing
--remote-debugging-port=... argument.  When an Electron app is run with
the --remote-debugging-port=... argument, running multiple instances
with the same --user-data-dir=... can cause malfunctions, and indeed
that's what happens with the Diagrams.net app.  If you want to run
multiple instances of Emacs each connected with an instance of a
Diagrams.net app, each Diagrams.net app should be run with a different
value for `emacs-drawio-relaunch-chrome-data'.")

(defvar emacs-drawio-relaunch--relaunch-in-progress nil)

(defvar emacs-drawio-relaunch--current-indium-chrome-port nil)
(defvar emacs-drawio-relaunch--current-dotindium-file nil)
(defvar emacs-drawio-relaunch--current-indium-client-port nil)

(defun emacs-drawio-generate-dotindium-json
    (port &optional parent-dir)
  "Generate .indium.json file interpolating PORT in template.
Return full path of file generated.
If PARENT-DIR is nil, a temporary directory is used."
  (cl-assert (numberp port))
  (let* ((tmp-dir (or parent-dir (make-temp-file "emacs-drawio" t)))
         (tmp-file (expand-file-name ".indium.json" tmp-dir))
         (default-directory tmp-dir)
         (save-now (current-time)))
    (make-directory tmp-dir t)
    (with-temp-file tmp-file
      (insert
       (format
        "{
  \"configurations\": [
    {
      \"name\": \"Web project\",
      \"type\": \"chrome\",
      \"port\": \"%d\",
      \"url\": \"http://192.168.22.1/myproject/index.html\"
    }
  ]
}
" port)))
    tmp-file
    ))

(defun emacs-drawio-relaunch--local-port-available (port)
  "Return nil if PORT is not in use."
  ;; `lsp--find-available-port' uses `open-network-stream' to check if
  ;; a port is in use or not.  The reason we are using lsof instead of
  ;; `open-network-stream' is that when the latter receives as
  ;; argument a port already in use by an existing Indium client, that
  ;; existing connection breaks.  We do not want to interfere with
  ;; existing connections.
  ;; (eq 0 (call-process "lsof" nil nil nil (format "-i:%d" port)))
  "Check if a port is in use."
  (condition-case nil
      (let ((process (make-network-process :name "port-test"
                                           :service port
                                           :host 'local
                                           :server nil
                                           :noquery t)))
        (delete-process process)
        nil) ; Port is not in use
    (file-error t)) ; Port is in use
  )

(defun emacs-drawio-relaunch--find-available-port (starting-port)
  "Find available port on localhost starting from STARTING-PORT."
  (let ((port starting-port))
    (while (not (emacs-drawio-relaunch--local-port-available port))
      (cl-incf port))
    port))

;; Unused after refactoring: keeping for use by external libs.
(cl-defun emacs-drawio-relaunch--client-connected-function ()
  "Function to be called after Indium finishes connecting."
  (unwind-protect (emacs-drawio-refresh--close-dialog-inject)
    (progn
      ;; Remove itself from hook: run only once per launch.
      (setq indium-client-connected-hook
            (remove #'emacs-drawio-relaunch--client-connected-function
                    indium-client-connected-hook))
      ;; Use `emacs-drawio-relaunch--relaunch-in-progress' to tell
      ;; `emacs-drawio-relaunch-compiling' (the function that
      ;; initially triggered the code path that brought here) that
      ;; relaunch has completed.
      (setq emacs-drawio-relaunch--relaunch-in-progress nil))))

(defun emacs-drawio-relaunch--get-indium-chrome-process ()
  "Get process named \"indium-chrome-process\".
This process name is specified in the function
`indium-launch-chrome'."
  (get-process "indium-chrome-process"))

(defun emacs-drawio-relaunch--indium-repl-live-p()
  "Return non-nil if indium repl is live."
  (let ((timeout 20)
        (counter 0)
        (donep nil)
        (livep nil))
    (when (indium-client-process-live-p)
      (while (and (not donep)
                  (< counter timeout))
        (ignore-errors
          (indium-eval
           "1"
           (lambda (iro)
             (setq
              livep (equal "1" (indium-remote-object-description iro))
              donep t))))
        (sit-for 0.05)
        (setq counter (1+ counter))))
    livep))

(defun emacs-drawio-relaunch--launch-and-wait-for-chrome-process ()
  "Relaunch Indium and wait for chrome process."
  (let ((indium-chrome-process-old
         (emacs-drawio-relaunch--get-indium-chrome-process)))
    (indium-quit)
    (indium-launch)
    ;; Wait for chrome process to start
    (let ((timeout 10)
          (sleep-counter 0))
      (while (and (equal
                   indium-chrome-process-old
                   (emacs-drawio-relaunch--get-indium-chrome-process))
                  (> timeout (setq sleep-counter (1+ sleep-counter))))
        (sit-for 1)))
    ;; Wait for chrome process to actually become ready:
    ;; 1. Especially on older hardware with non-SSD storage, it may
    ;;    takes a while for Electron-based apps to become ready after
    ;;    the process starts.
    ;; 2. This is a workaround to have a longer timeout than the
    ;;    10 retries with 500ms delay between each, hard-coded
    ;;    in Indium's js client. Hard-coded means: the function
    ;;    "connect" (indium/adapters/cdp/index.js) calls the
    ;;    function "doOrRetry" (indium/helpers/async.js) with
    ;;    default 2nd (retries=10) and 3rd (delay=500)
    ;;    arguments.
    (let ((timeout 100)
          (sleep-counter 0))
      (while (and (process-live-p
                   (emacs-drawio-relaunch--get-indium-chrome-process))
                  (not (emacs-drawio-relaunch--indium-repl-live-p))
                  (> timeout (setq sleep-counter (1+ sleep-counter))))
        (sit-for 1)
        ;; Manually quit and try to connect again, in case Indium's
        ;; timeout has run out.
        (indium-quit)
        (indium-connect)))))

(cl-defun emacs-drawio-relaunch
    (&optional
     ;; A user may want to manually specify a different port
     ;; (e.g. when another chrome/electron based application is
     ;; already running a debug session on on a port)
     (indium-drawio-chrome-port
      (emacs-drawio-relaunch--find-available-port
       indium-chrome-default-port)))
  "Relaunch drawio executable evaluating js files in \"dist\" dir.
This function temporarily sets indium variables for ports during
launch so that different instances of Emacs don't interfere with each
other.  Values used during launch are stored in vars whose name starts
with emacs-drawio-relaunch--current-indium-.

This function is only involved in launching the process with the
required args, so:
- it does not modify `indium-client-connected-hook'
- it does not inject js once the process is live."
  (cl-assert (or (stringp emacs-drawio-relaunch-chrome-data-dir)
                 (null emacs-drawio-relaunch-chrome-data-dir)))
  (let* (
         ;; The reason we need to find a free port ourselves for
         ;; `indium-client--process-port' instead of trying to use
         ;; `indium-connect' and seeing what happens is that when
         ;; `indium-connect' finds the address already in use it
         ;; becomes stuck retrying to connect and one needs to use
         ;; `keyboard-quit'.  I.e. we can't get an usable error or
         ;; message out of it.
         (indium-drawio-client-process-port
          (emacs-drawio-relaunch--find-available-port
           indium-client--process-port))
         (dotindium-file
          (emacs-drawio-generate-dotindium-json
           indium-drawio-chrome-port))
         ;; `indium-launch' needs `default-directory' to be set
         ;; to where .indium.js is located.
         (default-directory (file-name-directory dotindium-file))
         ;; Save Indium vars to restore them when finished.
         (save-indium-chrome-executable indium-chrome-executable)
         (save-indium-client-process-port indium-client--process-port)
         (save-indium-chrome-use-temporary-profile
          indium-chrome-use-temporary-profile)
         (save-indium-chrome-data-dir indium-chrome-data-dir)
         ;; Get indium process before launch so that we can compare it
         ;; later and see if launching has been happening.
         (indium-chrome-process-old
          (emacs-drawio-relaunch--get-indium-chrome-process))
         (indium-chrome--command-advice
          ;; [155391:0519/122244.243641:ERROR:devtools_http_handler.cc(766)]
          ;; Rejected an incoming WebSocket connection from the
          ;; http://127.0.0.1:9222 origin. Use the command line flag
          ;; --remote-allow-origins=http://127.0.0.1:9222 to allow
          ;; connections from this origin or --remote-allow-origins=*
          ;; to allow all origins.
          (lambda (cmdlist)
            (let ((executable (car cmdlist))
                  (old-args (cdr cmdlist))
                  (new-args
                   (list (format
                          "--remote-allow-origins='http://127.0.0.1:%d'"
                          indium-drawio-chrome-port))))
              `(,executable ,@new-args ,@old-args))))
         )
    ;; Do not set `indium-client-connected-hook': it's cleaner if this
    ;; function is only involved in launching the process.
    ;; (add-to-list 'indium-client-connected-hook
    ;;              #'emacs-drawio-relaunch--client-connected-function
    ;;              t)
    (setq indium-chrome-executable emacs-drawio-drawio-executable)
    (setq indium-client--process-port indium-drawio-client-process-port)
    ;; When `indium-chrome-use-temporary-profile' is non-nil and
    ;; `indium-chrome-data-dir' is nil:
    ;; 1. Indium passes to chrome process "--user-data-dir=nil".
    ;; 2. Diagrams.net uses this nil directory as its config dir. What
    ;;    we'd like instead is to let Diagrams.net use its default
    ;;    config dir.
    ;; 3. When `emacs-drawio-relaunch-chrome-data-dir' is nil we also
    ;;    set `indium-chrome-use-temporary-profile' to nil, so that
    ;;    Indium does not pass --user-data-dir=nil to the chrome
    ;;    process.
    (cond
     ((or (null emacs-drawio-relaunch-chrome-data-dir)
          (string-empty-p emacs-drawio-relaunch-chrome-data-dir))
      ;; When `indium-chrome-use-temporary-profile' is nil, this tell
      ;; indium to not pass --user-data-dir=... to chrome process
      ;; (see: `indium-chrome').
      (setq indium-chrome-use-temporary-profile nil))
     ((stringp emacs-drawio-relaunch-chrome-data-dir)
      (setq indium-chrome-data-dir
            emacs-drawio-relaunch-chrome-data-dir))
     (t (message
         (concat
          "[WARNING emacs-drawio-relaunch] Variable "
          " `emacs-drawio-relaunch-chrome-data-dir' has wrong type:\n"
          "- expected types: either string or nil. \n"
          "- actual type: %S \n"
          "- actual value: %S \n")
         (type-of emacs-drawio-relaunch-chrome-data-dir)
         emacs-drawio-relaunch-chrome-data-dir)))
    ;; We need to advice `indium-chrome--command' to pass additional
    ;; arguments the chromium executable.
    (advice-add #'indium-chrome--command
                :filter-return indium-chrome--command-advice)
    (unwind-protect
        (emacs-drawio-relaunch--launch-and-wait-for-chrome-process)
      (advice-remove #'indium-chrome--command
                     indium-chrome--command-advice)
      (unless (equal
               indium-chrome-process-old
               (emacs-drawio-relaunch--get-indium-chrome-process))
        ;; Chrome process started, update vars.
        (setq emacs-drawio-relaunch--current-indium-chrome-port
              indium-drawio-chrome-port)
        (setq emacs-drawio-relaunch--current-dotindium-file
              dotindium-file)
        (setq emacs-drawio-relaunch--current-indium-client-port
              indium-drawio-client-process-port))
      ;; Restore indium vars.
      (setq indium-chrome-executable save-indium-chrome-executable)
      (setq indium-client--process-port save-indium-client-process-port)
      (setq indium-chrome-use-temporary-profile
            save-indium-chrome-use-temporary-profile)
      (setq indium-chrome-data-dir save-indium-chrome-data-dir)
      )))

(defun emacs-drawio-relaunch-wait-indium-repl
    (&optional timeout interval callback)
  "If (indium-process-live-p) is non-nil, wait for REPL to go live.
Optional Argument TIMEOUT determines how long to wait for before
giving up.
Optional Argument INTERVAL determines how often to check for REPL.
Optional argument callback is a nullary function to be called after
REPL becomes live."
  (let ((live nil)
        (timeout (or timeout 50))
        (interval (or interval 0.2)))
    (while (and
            (> timeout 0)
            (indium-client-process-live-p)
            (not (setq live (emacs-drawio-relaunch--indium-repl-live-p))))
      (sit-for interval)
      (cl-decf timeout interval))
    (when (and live callback)
      (funcall callback))))

(defun emacs-drawio-relaunch--compilation-finish-function (_buf _desc)
  "Compilation-finish-function used by `emacs-drawio-relaunch'.
_BUF and _DESC are passed by Emacs when compilation finishes."
  ;; The reason why we are using a wait loop instead of
  ;; `indium-client-connected-hook', is because the editor can be
  ;; launched and become live before compilation has finished: in this
  ;; case a function added to `indium-client-connected-hook' would not
  ;; inject the result of the current compilation.
  ;; Instead, this function runs after compilation is finished: then
  ;; if the JS REPL is live, it's ok to inject.
  (unwind-protect
      (emacs-drawio-relaunch-wait-indium-repl
       50 0.2
       #'emacs-drawio-reload--inject-js
       ;; #'emacs-drawio-refresh--close-dialog-inject
       )
    (setq emacs-drawio-relaunch--relaunch-in-progress nil)
    ;; To avoid interfering with other compilations, remove self from
    ;; `compilation-finish-functions' after launching indium.
    (emacs-drawio-util--remove-compilation-finish-function
     #'emacs-drawio-relaunch--compilation-finish-function)))

(cl-defun emacs-drawio-relaunch-compiling
    (&optional (emacs-drawio-dir emacs-drawio-global-dir)
               (sync nil)
               ;; start-compile-watcher defaults to nil because when
               ;; (process-live-p emacs-drawio-compile-watcher) is
               ;; non-nil at startup, the while loop hangs Emacs.
               ;; I Don't know why.
               (start-compile-watcher nil))
  "Recompile js side, relaunch, eval recompiled js via indium."
  (interactive)
  (indium-quit)
  (cl-assert (file-exists-p emacs-drawio-drawio-executable))
  (let ((proc (emacs-drawio-relaunch--get-indium-chrome-process)))
    (when proc (kill-process proc)))
  ;; TODO: sync should wait for
  ;; `emacs-drawio-relaunch--compilation-finish-function' to
  ;; return. As it is now it just waits for compilation. compilation
  ;; finish function should have access to a variable that we can
  ;; observe from here.
  (setq emacs-drawio-relaunch--relaunch-in-progress t)
  (when (and start-compile-watcher
             (not (process-live-p emacs-drawio-compile-watcher)))
    (emacs-drawio-compile-watcher-start))
  (emacs-drawio-relaunch)
  ;; If a webpack watcher process is running, do not start a new
  ;; compilation, just wait for it to finish.
  (if (not (process-live-p emacs-drawio-compile-watcher))
      (emacs-drawio-compile
       #'emacs-drawio-relaunch--compilation-finish-function
       emacs-drawio-global-dir-js)
    (switch-to-buffer-other-window (emacs-drawio-compile-watcher-buffer-name))
    (while (not (emacs-drawio-compile-watcher-compilation-finished-p))
      (sit-for 0.3)))
  (when (and sync
             emacs-drawio-relaunch--relaunch-in-progress)
    (sit-for 0.1)))

(provide 'emacs-drawio-relaunch)
;;; emacs-drawio-relaunch.el ends here
