/**
 * Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { EditorUiCustomListener } from "../editorui/EditorUiCustomListener";
import { GraphModelListener } from "./GraphModelListener";
import { GraphSelectionModelListener } from "./GraphSelectionModelListener";

export class EmacsGraphManager {

    private static GRAPH_MANAGER_FIELD_NAME = "emacsGraphManager"

    graph: mxGraph;
    euiCustomListener: EditorUiCustomListener
    private enabled: boolean = false;
    graphModelListener = new GraphModelListener(this)
    graphSelectionModelListener = new GraphSelectionModelListener(this)

    constructor(graph: mxGraph, euiCustomListener: EditorUiCustomListener) {
        this.euiCustomListener = euiCustomListener;
        this.graph = graph;
    }

    isEnabled() {
        return this.enabled;
    }

    private setEnabled(boolean: boolean) {
        this.enabled = boolean;
    }

    enable() {
        this.install();
        this.graphModelListener.enable();
        this.graphSelectionModelListener.enable();
        this.setEnabled(true);
    }

    disable() {
        this.graphModelListener.disable();
        this.graphSelectionModelListener.disable();
        this.setEnabled(false);
    }

    install() {
        this.installGraphModelListener();
        this.installGraphSelectionModelListener();
    }

    uninstall() {
        this.disable();
        this.uninstallGraphModelListener();
        this.uninstallGraphSelectionModelListener();
    }

    installGraphModelListener() {
        const model = this.graph.getModel();
        for (let l of model.eventListeners) {
            if (l instanceof GraphModelListener) {
                return
            }
        }
        model.addListener(mxEvent.CHANGE, this.graphModelListener as any);
    }

    installGraphSelectionModelListener() {
        const selectionModel = this.graph.getSelectionModel();
        for (let l of selectionModel.eventListeners) {
            if (l instanceof GraphSelectionModelListener) {
                return
            }
        }
        selectionModel.addListener(mxEvent.CHANGE, this.graphSelectionModelListener as any);
    }

    uninstallGraphModelListener() {
        this.graph.getModel().removeListener(this.graphModelListener as any)
    }

    uninstallGraphSelectionModelListener() {
        this.graph.getSelectionModel().removeListener(this.graphSelectionModelListener as any)
    }

    static getGraphManager(graph: mxGraph): EmacsGraphManager | undefined {
        return graph[EmacsGraphManager.GRAPH_MANAGER_FIELD_NAME]
    }

    static setGraphManager(graph: mxGraph, emacsGraphManager: EmacsGraphManager): void {
        graph[EmacsGraphManager.GRAPH_MANAGER_FIELD_NAME] = emacsGraphManager
    }

    static getInstallEmacsGraphManager(graph: mxGraph, euiCustomListener: EditorUiCustomListener): EmacsGraphManager {
        if (typeof EmacsGraphManager.getGraphManager(graph) === "undefined") {
            EmacsGraphManager.setGraphManager(graph, new EmacsGraphManager(graph, euiCustomListener))
        }
        return EmacsGraphManager.getGraphManager(graph);
    }

    static uninstallEmacsGraphManager(graph: mxGraph): void {
        const emacsGraphManager = EmacsGraphManager.getGraphManager(graph);
        if (typeof emacsGraphManager !== "undefined") {
            emacsGraphManager.uninstall();
        }
    }

}
