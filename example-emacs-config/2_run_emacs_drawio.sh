#!/bin/env sh
HERE_DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"
RUN_EMACS_DRAWIO_EL="$HERE_DIR/run-emacs-drawio.el"

(cd "$HERE_DIR/../" && cask emacs -Q --load "$RUN_EMACS_DRAWIO_EL")
