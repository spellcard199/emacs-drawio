if ! command -v node > /dev/null; then
    echo "Not in PATH: node"
    exit 1
fi

if ! command -v npm > /dev/null; then
    echo "Not in path: npm"
    exit 1
fi

# ~900 MB (30 min)
git clone --recursive --depth=1 https://github.com/jgraph/drawio-desktop.git
cd drawio-desktop || exit 1
# ~500 MB (5 min)
npm install

allow_insecure_localhost_websocket () {
    sed -i \
        "s/\(connect-src \\\'self\\\'\)/\1 ws:\/\/localhost:*/" \
        drawio/src/main/webapp/index.html
}

run_app_with_remote_debugging_port() {
    port=$1
    npx electron \
        "drawio/src/main/webapp/electron.js" \
        --remote-debugging-port="$port"
}
