;;; emacs-drawio-data.el --- structured data types -*- lexical-binding: t; -*-

;; Copyright (C) 2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;

(require 'cl-lib)

;;; Code:

(cl-defstruct (emacs-drawio-data-diagram
               (:constructor emacs-drawio-data-diagram-create)
               (:copier nil))
  "Represents data in an mxfile."
  (pages nil
         :type list
         :documentation
         "List of `emacs-drawio-data-page'"))

(cl-defstruct (emacs-drawio-data-page
               (:constructor emacs-drawio-data-page-create)
               (:copier nil))
  "Represents data in an mxGraph."
  (page-name nil :type string)
  (cells-ht (make-hash-table :test 'equal)
            :type hash-table
            :documntation
            "Hash-table of either:
cell-id -> emacs-drawio-data-cell
cell-id -> string containing org-mode representation of cell"))

(cl-defstruct (emacs-drawio-data-cell
               (:constructor emacs-drawio-data-cell-create)
               (:copier nil))
  "Represents data in an mxCell."
  (id nil :type string)
  (geometry nil :type list)
  (style nil :type list)
  (value nil :type list))

(provide 'emacs-drawio-data)

;;; emacs-drawio-data.el ends here
