;;; package --- Functions to work with cells as org headings  -*- lexical-binding:t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:

(require 'json)
(require 'subr-x)
(require 'outline)
(require 'cl-lib)
(require 'emacs-drawio-data)

;;; Code:

;;; json -> org


(defun emacs-drawio-org-from-ejson-cell-style (cell-ejson-style)
  "Format CELL-EJSON-STYLE to org string."
  (mapconcat
   (lambda (kv-cons)
     (concat "- " (symbol-name (car kv-cons))
             "="
             (cdr kv-cons)))
   cell-ejson-style
   ;; (split-string cell-ejson-style ";" t)
   "\n"))

(defun emacs-drawio-org-from-ejson-cell (cell-ejson)
  "Format CELL-EJSON from ejson to org string."
  (let* ((cell-id         (cdr (assoc 'id         cell-ejson)))
         ;; (cell-source-id  (cdr (assoc 'sourceId   cell-ejson)))
         ;; (cell-target-id  (cdr (assoc 'targetId   cell-ejson)))
         ;; (cell-mxObjectId (cdr (assoc 'mxObjectId cell-ejson)))
         ;; (cell-visible    (cdr (assoc 'visible    cell-ejson)))
         ;; (cell-vertex     (cdr (assoc 'vertex     cell-ejson)))
         ;; (cell-edge       (cdr (assoc 'edge       cell-ejson)))
         (cell-style-maybe (cdr (assoc 'style      cell-ejson)))
         (cell-geometry    (cdr (assoc 'geometry   cell-ejson)))
         (cell-value       (cdr (assoc 'value      cell-ejson)))

         (cell-org-heading     (concat "**** " cell-id))
         (cell-org-value-attrs
          (if (listp cell-value)
              (let ((attrs (cdar cell-value)))
                ;; (setq y attrs)
                (mapconcat
                 (lambda (attr)
                   (let ((attr-name  (symbol-name (car attr)))
                         (attr-value (cdr attr)))
                     (concat "***** " attr-name
                             "\n\n" attr-value)))
                 attrs
                 "\n\n"))
            (concat "***** label\n\n" cell-value)))
         (cell-org-style
          (concat
           (propertize "***** style\n"
                       'read-only t)
           "\n"
           (if cell-style-maybe
               (emacs-drawio-org-from-ejson-cell-style
                cell-style-maybe))))
         (cell-org-geometry
          (concat
           (propertize "***** geometry\n"
                       'read-only t)
           "\n"
           (prin1-to-string cell-geometry))))
    (concat (propertize (concat cell-org-heading "\n")
                        'read-only t)
            cell-org-value-attrs "\n\n"
            (concat cell-org-style "\n\n")
            cell-org-geometry)))

(defun emacs-drawio-org-from-ejson-page (page-ejson)
  "Format PAGE-EJSON to org string."
  (cl-check-type page-ejson list)
  (let ((page-name  (symbol-name (car page-ejson)))
        (page-cells (cdr page-ejson)))
    (concat
     (propertize (concat "*** " page-name "\n")
                 'read-only t)
     (mapconcat #'emacs-drawio-org-from-ejson-cell
                page-cells
                "\n\n"))))

(defun emacs-drawio-org-from-ejson-doc (diagram-ejson)
  "Format DIAGRAM-EJSON to org string."
  (cl-check-type diagram-ejson list)
  (mapconcat #'emacs-drawio-org-from-ejson-page
             diagram-ejson
             "\n\n"))

;;; org -> structured data

(defun emacs-drawio-org-parse-org-cell-geometry (org-cell-geometry-heading)
  "Parse ORG-CELL-GEOMETRY-HEADING into elisp data."
  (cl-assert (string-prefix-p "geometry\n" org-cell-geometry-heading))
  (car (read-from-string
        (string-trim
         (string-remove-prefix "geometry\n" org-cell-geometry-heading)))))

(defun emacs-drawio-org-parse-org-cell-style (org-cell-style-heading)
  "Parse ORG-CELL-STYLE-HEADING into elisp data."
  (cl-assert (string-prefix-p "style\n" org-cell-style-heading))
  (mapcar
   (lambda (line)
     (let* ((splitted (split-string line "=")))
       `(,(car splitted) . ,(nth 1 splitted))))
   (split-string
    (replace-regexp-in-string
     "^- " "" (string-remove-prefix "style\n" org-cell-style-heading))
    "\n" t " ")))

(defun emacs-drawio-org-parse-org-cell-attr (org-cell-attr-heading)
  "Parse ORG-CELL-ATTR-HEADING into elisp data."
  (cl-assert (not (string-prefix-p "geometry\n" org-cell-attr-heading)))
  (cl-assert (not (string-prefix-p "style\n" org-cell-attr-heading)))
  (let* ((attr-name (progn (string-match "\\(.*?\\)\n" org-cell-attr-heading)
                           (match-string 1 org-cell-attr-heading)))
         (attr-value (string-trim
                      (string-remove-prefix attr-name
                                            org-cell-attr-heading))))
    (cons attr-name attr-value)))

(defun emacs-drawio-org-parse-org-cell-id (org-cell)
  "Parse cell id from ORG-CELL."
  (cl-check-type org-cell string)
  (setq debug-on-error 1)
  (string-trim
   (substring org-cell 0 (string-match "\n" org-cell))
   "\\*\\*\\*\\* "))

(defun emacs-drawio-org-parse-org-cell (org-cell)
  "Parse ORG-CELL into `emacs-drawio-data-cell'."
  (cl-check-type org-cell string)
  (let* ((cell-id (emacs-drawio-org-parse-org-cell-id org-cell))
         (org-cell-splitted
          (split-string org-cell
                        "^\\*\\*\\*\\*\\* " nil))
         ;; (cell-id (string-trim (car org-cell-splitted) "**** "))
         (org-cell-subheadings (cdr org-cell-splitted))
         ;; These are extended in the dolist block.
         (cell-attributes)
         (cell-style)
         (cell-geometry))
    (dolist (subheading org-cell-subheadings)
      (cond
       ((string-prefix-p "geometry\n" subheading)
        (setq cell-geometry (emacs-drawio-org-parse-org-cell-geometry subheading)))
       ((string-prefix-p "style\n" subheading)
        (setq cell-style (emacs-drawio-org-parse-org-cell-style subheading)))
       (t (let* ((cell-attr-parsed
                  (emacs-drawio-org-parse-org-cell-attr subheading))
                 (attr-name (car cell-attr-parsed))
                 (attr-value (cdr cell-attr-parsed)))
            (push `(,(intern attr-name) . ,attr-value)
                  cell-attributes)))))
    (emacs-drawio-data-cell-create
     :id cell-id
     :style cell-style
     :geometry cell-geometry
     :value (cons 'attributes (reverse cell-attributes)))))

(cl-defmethod emacs-drawio-org-data-page--add-cell
  ((page-data emacs-drawio-data-page)
   (cell-string string))
  "Add CELL-STRING to PAGE-DATA."
  (puthash (emacs-drawio-org-parse-org-cell-id cell-string)
           cell-string
           (emacs-drawio-data-page-cells-ht page-data)))

(defun emacs-drawio-org-parse-org-page (org-page)
  "Parse ORG-PAGE, an org fragment corresponding to a mxGraph graph."
  (cl-check-type org-page string)
  (let* ((org-page-splitted
          (split-string org-page "^\\*\\*\\*\\* " nil))
         (page-name (string-trim (car org-page-splitted)))
         (org-cells (cdr org-page-splitted))
         (page-data (emacs-drawio-data-page-create
                     :page-name page-name
                     :cells-ht (make-hash-table
                                :test 'equal
                                :size (length org-cells)))))
    (dolist (org-cell org-cells page-data)
      (emacs-drawio-org-data-page--add-cell page-data org-cell))))

(defun emacs-drawio-org-parse-org-doc (org-doc)
  "Parse ORG-DOC into `emacs-drawio-data-diagram'."
  (cl-check-type org-doc string)
  (emacs-drawio-data-diagram-create
   :pages (mapcar #'emacs-drawio-org-parse-org-page
                  (split-string org-doc "^\\*\\*\\* " t))))



(provide 'emacs-drawio-org)
;;; emacs-drawio-org ends here
