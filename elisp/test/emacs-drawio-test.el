;;; emacs-drawio-test.el --- tests for `emacs-drawio's elisp side -*- lexical-binding:t -*-

;; Copyright (C) 2020-2021, spellcard199 <spellcard199@protonmail.com>

;; SPDX-License-Identifier: GPL-3.0-or-later

;;; Commentary:
;; 

(require 'buttercup)

(require 'emacs-drawio-global)
(require 'emacs-drawio-relaunch)
(require 'indiumeval)

;;; Code:

;; (emacs-drawio-test-re-seq :: String -> String -> Integer -> [String])
(cl-defun emacs-drawio-test-re-seq (regexp string &optional (group-num 0))
  "Get a list of all regexp matches in a string"
  ;; Adapted from:
  ;; https://emacs.stackexchange.com/questions/7148/get-all-regexp-matches-in-buffer-as-a-list
  (save-match-data
    (let ((pos 0)
          matches)
      (while (string-match regexp string pos)
        (push (match-string group-num string) matches)
        (setq pos (match-end group-num)))
      matches)))

;; (emacs-drawio-test-indiumeval-call--jsfuncs-in-file :: String -> [String])
(defun emacs-drawio-test-indiumeval-call--jsfuncs-in-file (file)
  "Argument FILE is a file to search pattern in."
  (with-temp-buffer
    (insert-file-contents file)
    (emacs-drawio-test-re-seq
     "indiumeval-call\\(-sync\\)?[[:blank:]\n]+\"\\(.*?\\)\""
     (buffer-substring-no-properties
      (point-min)
      (point-max))
     2)))

;; (emacs-drawio-test-indiumeval-call--jsfuncs-in-files :: [String] -> [String])
(defun emacs-drawio-test-indiumeval-call--jsfuncs-in-files (files)
  "Argument FILES is a list of files to search the pattern in."
  (apply 'append
         (mapcar
          #'emacs-drawio-test-indiumeval-call--jsfuncs-in-file
          files)))

(defun js-func-defined-in-repl-p (js-func-name)
  "Check if JS-FUNC-NAME is defined in Indium's JS REPL."
  (if (string-equal "function"
                    (indium-remote-object-to-string
                     (indiumeval-eval-sync js-func-name)))
      t nil))


(describe
    ""

  (before-all nil)

  (before-each nil)

  (it "can launch diagrams.net app."
    (expect
     (progn
       (emacs-drawio-relaunch emacs-drawio-global-dir t)
       (string-equal "true"
                     (indium-remote-object-to-string
                      (indiumeval-eval-sync "true"))))))

  (it "can confirm that all functions mentioned by `indiumeval-call...' are defined in Indium's JS REPL."
    (expect
     (let* ((elisp-files (directory-files
                          (concat emacs-drawio-global-dir "elisp")
                          t
                          "^emacs-drawio-?[a-z]*.el$"))
            (js-func-names (emacs-drawio-test-indiumeval-call--jsfuncs-in-files
                            elisp-files)))
       (eval (cons 'and (mapc #'js-func-defined-in-repl-p
                              js-func-names))))))
  ;; (it "TODO" (expect t))

  )

(provide 'emacs-drawio-test)
;;; emacs-drawio-test.el ends here
