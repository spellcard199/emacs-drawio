/**
 * Copyright (C) 2021-2023, spellcard199 <spellcard199@protonmail.com>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import * as utilDrawio from "drawio-plugin-eight-droves-of-rabbits/src/util"
import { cellGeometrySetPreferredSizeIfAutosizeAndNotGroup } from "drawio-plugin-eight-droves-of-rabbits/src/util/mxgraph/cell/cellgeometry";

export type CellData = {
    doRemoveCell: boolean;
    edge: boolean;
    id: string;
    geometry: mxGeometry;
    mxObjectId: string;
    parent: string | null;
    sourceId: string | null;
    style: string;
    targetId: string | null;
    value: any;
    vertex: boolean;
    visible: boolean;
}

export type PagesData = {
    (pageName: string): Array<CellData>
}

export function getRawXmlFileDataStr(editorUi: any) {
    // "Raw" refers to diagrams being not deflated/encoded.
    if (editorUi.currentFile.fileObject !== undefined) {
        const filePath = editorUi.currentFile.fileObject.path;
        // @ts-ignore
        const fileData = mxUtils.getPrettyXml(
            utilDrawio.parse.drawio.getRawXmlFileDataClone(
                editorUi.getXmlFileData()
            )
        );
        return JSON.stringify(
            {
                path: filePath,
                data: fileData,
            }
        )
    } else {
        return null;
    };
}

export function styleGetImageBoundsInString(cellStyle: string): { start: number, end: number } | null {
    var imageInStyle = cellStyle.indexOf(";image=");
    if (imageInStyle !== -1) {
        const imageIndexStart = imageInStyle + 1 // exclude semicolon
        const endMaybe = cellStyle.indexOf(";", imageIndexStart + 1);
        const imageIndexEnd = endMaybe > -1 ? endMaybe : cellStyle.length;
        return {
            start: imageIndexStart,
            end: imageIndexEnd,
        }
    } else {
        return null;
    }
}

export const IMAGE_PLACEHOLDER = "<IMAGE_PLACEHOLDER>"

export function cellToCellData(cell: mxCell): CellData {
    "Gets a subset of cell attrs, to be stringifiable"
    // TODO: add "parent" field.

    // The reason we are not using editorUi.getCellForJson is so we may
    // customize the data and the format we include. For example for a
    // simple vertex editorUi.getCellForJson returns something like:
    // {
    //   "id": "Ll_oeiGsfg7qgr_SC9KF-4",
    //   "vertex": 1,
    //   "parent": "1",
    //   "geometry": "<mxGeometry x=\"340\" y=\"215\" width=\"80\" height=\"30\"/>",
    //   "xmlValue": "<object label=\"Lorem ipsum\" prop1=\"prop value\"/>"
    // }
    // If you wanted to edit the properties from Emacs you would have to
    // use a second xml parser to parse the xmlValue object. This function instead
    // would return something like:
    // {
    //   "id": "Ll_oeiGsfg7qgr_SC9KF-4",
    //   "geometry": {
    //     "x": 340,
    //     "y": 215,
    //     "width": 80,
    //     "height": 30
    //   },
    //   "sourceId": null,
    //   "targetId": null,
    //   "mxObjectId": "mxCell#8596",
    //   "visible": true,
    //   "vertex": true,
    //   "edge": false,
    //   "value": {
    //     "attributes": {
    //       "label": "foobar",
    //       "prop1": "prop value"
    //     }
    //   }
    // }

    const geo = cell.getGeometry();
    let value: string | object;
    if (cell.value === null) {
        value = "";
    } else if (typeof cell.value === "string") {
        value = utilDrawio.cellvalue.stringFromLabel(cell.value);
    } else if (typeof cell.value === "object") {
        var attrs = {}
        Array.from(cell.value.attributes).forEach(
            (attr: Attr) => attrs[attr.name] = attr.name === 'label'
                ? utilDrawio.cellvalue.stringFromLabel(attr.value)
                : attr.value
        );
        value = { attributes: attrs }
    }

    const styleObj = {}
    cell.style.split(";").forEach((styleEntry: string) => {
        const equalCharIndex = styleEntry.trim().indexOf("=");
        if (equalCharIndex !== -1) {
            const k = styleEntry.substring(0, equalCharIndex);
            const v = styleEntry.substring(equalCharIndex + 1);
            if (k === "image") {
                styleObj[k] = IMAGE_PLACEHOLDER;
            } else {
                styleObj[k] = v
            }
        }
    })

    const obj: any = {
        edge: cell.edge,
        id: cell.id,
        geometry: {
            x: geo.x,
            y: geo.y,
            width: geo.width,
            height: geo.height,
        },
        mxObjectId: cell.mxObjectId,
        parent: cell.getParent() ? cell.getParent().id : null,
        sourceId: cell.source ? cell.source.getId() : null,
        style: styleObj,
        targetId: cell.target ? cell.target.getId() : null,
        value: value,
        vertex: cell.vertex,
        visible: cell.visible,
    };
    return obj;
}

export function getSelectionCellsData(editorui): object {
    const pageName: string = editorui.currentPage.getName();
    const cellsData = editorui.editor.graph.getSelectionCells().map(cellToCellData);
    const data = {};
    data[pageName] = cellsData;
    return data;
}

export function getSelectionCellsDataStringified(editorui: any, graphSetEnabled: boolean)
    : String {
    return JSON.stringify(
        getSelectionCellsData(editorui), null, 2
    )
}

export function mkHTMLObjectElem(): HTMLObjectElement {
    const xmldoc: XMLDocument = mxUtils.createXmlDocument();
    return xmldoc.createElement("object");
}

export function setHTMLObjectElemAttrs(htmlObj: HTMLObjectElement, attrs: any) {
    Object.entries(attrs).forEach(entry => {
        // TODO: If attribute does not exists create it.
        const attrName: string = entry[0] as string;
        const attrValue: string = entry[1] as string;
        htmlObj.setAttribute(attrName, attrValue);
    })
}

export function updateCell(graph: mxGraph, cell: mxCell, data: CellData) {
    "Gets a subset of cell attrs, to be stringifiable."

    // If cell should be removed there is no need to set data.
    if (data.doRemoveCell) {
        graph.model.beginUpdate();
        try { graph.model.remove(cell); }
        finally { graph.model.endUpdate(); }
        return;
    }

    graph.model.beginUpdate();
    try {
        if (data.id) {
            cell.id = data.id;
        }
        if (data.geometry) {
            var geo = data.geometry;
            cell.geometry.x = geo.x;
            cell.geometry.y = geo.y;
            cell.geometry.width = geo.width;
            cell.geometry.height = geo.height;
        }
        if (data.sourceId) {
            cell.source = graph.model.getCell(data.sourceId);
        }
        if (data.targetId) {
            cell.target = graph.model.getCell(data.targetId);
        }
        if (data.mxObjectId) {
            cell.mxObjectId = data.mxObjectId;
        }
        if (data.visible) {
            cell.visible = data.visible;
        }
        if (data.vertex) {
            cell.vertex = data.vertex;
        }
        if (data.edge) {
            cell.edge = data.edge;
        }
        if (data.style) {
            var styleString = ""
            Object.entries(data.style).forEach((styleEntry) => {
                const k: string = styleEntry[0] as string;
                const v: string | null = styleEntry[1] as string | null;
                if (k !== "image") {
                    // Skip 'image' key: unsupported.
                    styleString += k;
                    if (v !== null) {
                        styleString += '=' + v;
                    }
                    styleString += ';';
                }
            });
            // Editing image's base64 from Emacs from the org buffer is
            // too slow/dangerous, so we do not support it: here we are
            // reusing image in cell.style.
            // styleGetImageBoundsInString includes 'image=...'
            const imageStartEndMaybe = styleGetImageBoundsInString(cell.style);
            if (imageStartEndMaybe !== null) {
                const imageFromCell = cell.style.substring(imageStartEndMaybe.start, imageStartEndMaybe.end);
                styleString += imageFromCell + ";";
            }
            graph.model.setStyle(cell, styleString);
        }
        if (data.value) {
            if (typeof cell.value === "string" && typeof data.value === "string") {
                // Cell has not properties, so cell.value can be just a string.
                graph.getModel().setValue(cell, utilDrawio.cellvalue.labelFromString(data.value))
            } else if (data.value.attributes) {
                // Cell has properties, so cell.value has to be an Element where
                // label and properties are Element's attributes.
                // Set cell properties.
                const newAttributes: { [attrName: string]: string } = data.value.attributes
                utilDrawio.cellvalue.cellValueSetAttributes(graph, cell, newAttributes)
                const cellAttributes: NamedNodeMap = cell.value.attributes
                // If in data.valua.attributes there is only a "label"
                // attribute, cellValueSetAttributes sets cell.value to a
                // string, so cell.value.attributes is undefind.
                if (cellAttributes && cellAttributes.length > 0) {
                    // Remove deleted cell properties.
                    for (let i = cellAttributes.length - 1; i >= 0; i--) {
                        const attrName = cellAttributes[i].name
                        if (!(attrName in data.value.attributes)) {
                            cellAttributes.removeNamedItem(attrName)
                        }
                    }
                }
            } else {
                throw new Error("[ERROR] Unexpected type for cell.value.")
            }
            // Apply autosize
            const autosize = data.style['autosize'];
            cellGeometrySetPreferredSizeIfAutosizeAndNotGroup(graph, [cell], true)
        }
    } finally {
        graph.model.endUpdate();
    }
}

export function updateCells(graph: mxGraph, cellsData: Array<CellData>) {
    graph.getModel().beginUpdate()
    try {
        for (var i in cellsData) {
            const cellData = cellsData[i];
            const cellId = cellData['id'];
            const cell: mxCell = graph.model.cells[cellId];
            updateCell(graph, cell, cellData);
            graph.view.clear(cell, cell == null);
            // Commented out because if ignoreChildren is:
            // - true: containers get resized to fit label, leaving
            //   children out of group's bounds
            // - false: unwanted behaviors happen:
            //   - children containers stop being children
            //   - cell gets moved to the right
            // graph.updateCellSize(cell, true);
        }
        // TODO: if the current solution continues working, remove this.
        // NOTE: graph.refresh() calls graph.view.validate();
        // graph.view.validate is the most expensive step and calling it on a
        // single cell doesn't save much. Instead of doing...:
        //   graph.view.validate(cell);
        // ... it's better to call it just once when done applying changes.
        // The reason we are not using graph.refresh() is that it calls
        // graph.view.clear(...) and we already cleared modified cells while
        // iterating: no need to iterate again.
        // Adapted from mxGraph.prototype.refresh:
        // graph.view.validate();
        // graph.sizeDidChange();
        // graph.fireEvent(new mxEventObject(mxEvent.REFRESH), null);
    } finally {
        graph.getModel().endUpdate()
    }
}

export function updatePages(editorUi: any, pagesData: PagesData) {
    const graph: mxGraph = editorUi.editor.graph;
    // FIXME: doesn't a page have its own graph? Here we are squashing pages
    // into a single graph.
    for (var pageName in pagesData) {
        const cellsData = pagesData[pageName];
        updateCells(graph, cellsData);
    }
}

export let getEditorFileUrlOrPath = utilDrawio.drawio.getEditorFileUrlOrPath