;;; emacs-drawio-compile.el --- Functions related to compiling ts to js -*- lexical-binding:t -*-

;; Copyright (C) 2020-2022, spellcard199 <spellcard199@protonmail.com>
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This file is not part of GNU Emacs.

;;; Commentary:
;;

(require 'emacs-drawio-global)
(require 'emacs-drawio-util)
(require 'term)

;;; Code:

;;;; Begin of emacs-drawio-compile-watcher

(defconst emacs-drawio-compile-webpack-executable
  (expand-file-name  "node_modules/webpack-cli/bin/cli.js"
                     emacs-drawio-global-dir-js))
(defvar emacs-drawio-compile-watcher nil)
(defvar emacs-drawio-compile-watcher-name "emacs-drawio-compile-watcher")


(cl-defun emacs-drawio-compile-npm--dependencies-installed-p
    (&optional (js-dir emacs-drawio-global-dir-js))
  "Estimate and return non-nil if node dependencies are installed.
Argument JS-DIR is the directory where node_modules is located."
  (file-exists-p
   (expand-file-name "node_modules/webpack-cli/bin/cli.js" js-dir)))

(defun emacs-drawio-compile-npm-install (&optional callback)
  "Install js dependencies with npm.
When the process in term exits, if CALLBACK is non-nil it's
called with PROCESS and EVENT as arguments.  PROCESS and EVENT are the
same arguments passed for the last time to the process sentinel.  Read
also `set-process-sentinel'."
  (interactive)
  (let* ((default-directory emacs-drawio-global-dir-js)
         (buffer (ansi-term "sh" "npm install"))
         (process (get-buffer-process buffer)))
    ;; Using term instead of compile to avoid output buffer being
    ;; polluted by all progress bar updates.
    (set-process-sentinel
     process
     (lambda (process event)
       (term-sentinel process event)
       (when (and (not (process-live-p process))
                  callback)
         (funcall callback process event))))
    (term-send-string process "npm install; exit $?\n")))

(defun emacs-drawio-compile-watcher-buffer-name ()
  (concat "*" emacs-drawio-compile-watcher-name "*"))

(cl-defun emacs-drawio-compile-watcher-start
    (&optional (emacs-drawio-compile-default-directory emacs-drawio-global-dir-js)
               (webpack-executable emacs-drawio-compile-webpack-executable))
  "Start webpack watcher as a new subprocess.
This is an alternative approach to `emacs-drawio-compile' that avoids webpack
startup times.  May be useful when exploring things in typescript."
  (if (not (emacs-drawio-compile-npm--dependencies-installed-p))
      ;; If node dependencies are not installed, run npm install and
      ;; call itself after installation finishes.
      (emacs-drawio-compile-npm-install
       (lambda (_process event_on_exit)
         (when (string-equal event_on_exit "finished\n")
           (emacs-drawio-compile-watcher-start
            emacs-drawio-compile-default-directory
            webpack-executable))))
    ;; If node dependencies are installed, proceed with compilation.
    (let* ((proc-name emacs-drawio-compile-watcher-name)
           (buffer-name (emacs-drawio-compile-watcher-buffer-name)))
      (when (process-live-p emacs-drawio-compile-watcher)
        ;; Clean up
        (kill-process emacs-drawio-compile-watcher)
        (with-current-buffer buffer-name
          (delete-region (point-min) (point-max))))
      (setq emacs-drawio-compile-watcher
            (let ((default-directory emacs-drawio-compile-default-directory))
              (start-process proc-name
                             buffer-name
                             webpack-executable
                             "--watch")))
      (with-current-buffer buffer-name
        ;; If we don't enable font-lock-mode the result is without ansi
        ;; escapes but also without colors. Maybe make this an option?
        (font-lock-mode 1))
      (set-process-filter emacs-drawio-compile-watcher
                          #'emacs-drawio-compile--echo-output-filter))))

(defun emacs-drawio-compile-watcher-compilation-finished-p ()
  "Return non-nil if `emacs-drawio-compile-watcher' has finished compiling."
  (when (process-live-p emacs-drawio-compile-watcher)
    (with-current-buffer (emacs-drawio-compile-watcher-buffer-name)
      (save-excursion
        (goto-char (point-max))
        (when (search-backward "No issues found." nil t)
          (forward-line 5)
          (string-equal "" (buffer-substring-no-properties
                            (point) (point-max))))))))

;;;; End of emacs-drawio-compile-watcher

(defun emacs-drawio-compile-compilation-finish-func-require-hack (buf _desc)
  "Dirty hack to avoid \"require() is not defined\" error.
Error is due to Electron and Drawio having disabled node integration."
  (with-current-buffer buf
    (shell-command "sed -E -i 's/(module.exports = require.*?);;/try {\\n \\1 \\n} catch (e) {\\n module.exports = undefined; console.log(`[EMACS-DRAWIO-HACK] ` + `\\1` + `\\n  ` + e);\\n}/' dist/bundle.js"))
  (setq compilation-finish-functions
        (delete
         'emacs-drawio-compile-compilation-finish-func-require-hack
         compilation-finish-functions)))

(cl-defun emacs-drawio-compile
    (&optional finish-func
               (emacs-drawio-compile-default-directory emacs-drawio-global-dir-js)
               (webpack-executable emacs-drawio-compile-webpack-executable)
               (sync nil))
  (cl-assert (or (null finish-func)
                 (functionp finish-func)))
  (if (not (emacs-drawio-compile-npm--dependencies-installed-p))
      ;; If node dependencies are not installed, run npm install and
      ;; call itself after installation finishes.
      (emacs-drawio-compile-npm-install
       (lambda (_process event_on_exit)
         (when (string-equal event_on_exit "finished\n")
           (emacs-drawio-compile
            finish-func
            emacs-drawio-compile-default-directory
            webpack-executable
            sync))))

    (add-hook 'compilation-finish-functions
              #'emacs-drawio-compile-compilation-finish-func-require-hack)

    (when finish-func
      (add-hook 'compilation-finish-functions
                finish-func t))
    (let* ((default-directory emacs-drawio-compile-default-directory)
           ;; TODO: would this work too?
           ;; (compilation-directory emacs-drawio-global-dir)
           (save-buffer (current-buffer))
           (compile-buffer (compile webpack-executable)))
      (when compile-buffer
        (switch-to-buffer-other-window compile-buffer)
        (goto-char (point-max))
        (switch-to-buffer-other-window save-buffer)
        (when sync
          (with-current-buffer compile-buffer
            (while
                (process-live-p (get-buffer-process (current-buffer)))

              ;;(not (save-excursion
              ;;      (goto-char (point-max))
              ;;      (search-backward "Compilation finished" nil t 1)))

              (sit-for 0.5))))
        ))))

(defun emacs-drawio-compile--echo-output-filter (proc string)
  ;; Adapted from: https://emacs.stackexchange.com/a/33274
  "Process filter to colorize ansi escapes."
  (when (buffer-live-p (process-buffer proc))
    (with-current-buffer (process-buffer proc)
      (let ((moving (= (point) (process-mark proc))))
        (save-excursion
          ;; Insert the text, advancing the process marker.
          (goto-char (process-mark proc))
          (insert (ansi-color-apply string))
          (set-marker (process-mark proc) (point)))
        (if moving (goto-char (process-mark proc)))))))

(provide 'emacs-drawio-compile)

;;; emacs-drawio-compile.el ends here
