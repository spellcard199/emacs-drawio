#!/bin/env sh
VERSION=13.9.9
FILE_NAME="draw.io-x86_64-${VERSION}.AppImage"

CUR_DIR="$(pwd)"
HERE_DIR="$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )"
DEST_FILE="$HERE_DIR/$FILE_NAME"
EXPECTED_MD5="44030c8b22152251149239fb2318eae5"

echo
echo "Downloading Draw.io desktop app to:"
echo "$(realpath $DEST_FILE)"
echo

wget -O "$DEST_FILE" -c "https://github.com/jgraph/drawio-desktop/releases/download/v${VERSION}/${FILE_NAME}"
download_success="$?"
actual_md5="$(md5sum "$DEST_FILE" | cut -d ' ' -f 1)"

if [ ! "$download_success" = 0 ]; then
    echo
    echo "[ERROR] wget returned an error while downloading Draw.io desktop app."
    echo
elif [ ! -f "$DEST_FILE" ]; then
    echo
    echo "[ERROR] draw.io desktop app wasn't downloaded."
    echo
elif [ ! "$EXPECTED_MD5" = "$actual_md5" ]; then
    echo
    echo "vvvvvvvvvv [WARNING] vvvvvvvvvv"
    echo "MD5 CHECKSUM ON DOWNLOADED DRAW.IO DESKTO APP FAILED:"
    echo "- expected: $EXPECTED_MD5"
    echo "- actual  : $actual_md5"
    echo "^^^^^^^^^^ [WARNING] ^^^^^^^^^^"
    echo
else
    echo
    echo "[SUCCESS] Draw.io desktop app was successfully downloaded to:"
    echo "$(realpath $DEST_FILE)"
    echo
    # If you don't make the app executable, indium-launch fails to start
    chmod a+x "$DEST_FILE"
fi

cd "$CUR_DIR"
