;;; emacs-drawio-mode-keybinding.el --- Keybindings for emacs-drawio-mode.el

;;; Commentary:
;; This file is separate from emacs-drawio.el to avoid circuar
;; dependencies while keeping all keybindings in one place.

(require 'emacs-drawio-mode)
(require 'emacs-drawio-eaas)

;;; Code:

(define-key emacs-drawio-mode-map
  (kbd "C-x C-s") #'emacs-drawio-eaas-save-file)

(define-key emacs-drawio-mode-map
  (kbd "M-h") #'emacs-drawio-mode-paragraph-fill-decrease)

(define-key emacs-drawio-mode-map
  (kbd "M-l") #'emacs-drawio-mode-paragraph-fill-increase)

(provide 'emacs-drawio-mode-keybinding)

;;; emacs-drawio-mode-keybinding.el ends here
