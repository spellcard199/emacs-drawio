#!/usr/bin/env bash

SCRIPTS_DIR="$(dirname ${BASH_SOURCE[0]})"

(
    # Without --install-links, some dependencies (e.g.
    # chrome-remote-interface) are not installed on node v18.16.0.
    cd "$SCRIPTS_DIR"/../js \
        && npm i --install-links
)
